# pg_qualstats



## 安装

本次活动开发是基于[ *pg_qualstatus* ](https://github.com/powa-team/pg_qualstats)插件修改的,若想体验该插件，请先安装[ *hypopg* ](https://github.com/HypoPG/hypopg)和[ *pg_cron* ](https://github.com/citusdata/pg_cron)插件,并在想要启用该插件的数据库中启用上述插件。



安装完成后，在postgresql.conf添加如下参数：
```
#add_to postgresql.conf:
shared_preload_libraries = 'pg_stat_statements,pg_qualstats,pg_cron'
pg_qualstats.result_directory = '存放推荐过程文件的绝对路径'

#确保 cron.database_name 中的 database 已经创建
cron.database_name = '要启用插件的数据库'  
```

即可正常使用。



## 改进

该插件在原插件的基础上，实现了以下功能：
- 推荐index的自动化，执行query后，调用 ```pg_qualstats_change()``` 即可自动建立index，并将推荐过程的信息存储到用户指定的目录下。

- 相比原插件，现在的推荐机制会考虑query的执行次数，并且会将已涉及index的query纳入考量，推荐效果更好。

- 增添预采样机制，相比原先以概率为标准随机收集的机制更加全面，负担更小。预采样表会收集query的CmdType、queryid、执行次数和总用时。用户可以选择是否开启预采样机制，若不开启，则会收集所有query的执行信息；若开启，用户可以指定每隔x时间，将预采样表中总用时在前n名，且用时占比超过z%的query登记为需要被详细统计。

  

## 配置
原先插件的*pg_qualstats.sample_rate*参数不再起作用。

以下是一些新增的参数：

- *pg_qualstats.pre_sample_max*(int, default 1000): 预采样表中tuple数的最大值
- *pg_qualstats.all_collect*(boolean, default false): 是否开启预采样机制
- *pg_qualstats.sample_minutes*(int, default 5): 预采样选取间隔分钟，最小值为1，最大值为60，要想让间隔时间超过60分钟，请查看 ```pg_qualstats_sample_time()``` 的信息。
- *pg_qualstats.sample_ranking*(int, default 5): 预采样每次选取前 *sample_ranking* 个query详细统计。
- *pg_qualstats.sample_proportion*(double, default 0.2): 预采样选取的query中，只有总耗时占比超过 *sample_proportion* 的才会被详细统计，否则继续留在预采样表中。



## 函数

以下是一些新增的函数：

- **pg_qualstats_change()**: 执行一次Index推荐，自动生成相应的Index，并且将推荐过程记录在指定文件中。
- **pg_qualstats_sample_time(timestr)**: 更改预采样选取的间隔时间，timestr要符合 *pg_cron* 的语法规则。
- **pg_qualstats_sample()**: 返回预采样表的内容。
- **pg_qualstats_ranking(int)**: 修改 ```pg_qualstats.sample_ranking``` 的值。
- **pg_qualstats_proportion(double)**: 修改 ```pg_qualstats.sample_proportion``` 的值。
- **pg_qualstats_collect()**: 决定启用预采样机制。
