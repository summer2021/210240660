/*-------------------------------------------------------------------------
 *
 * pg_qualstats.c
 *		Track frequently used quals.
 *
 * This extension works by installing a hooks on executor.
 * The ExecutorStart hook will enable some instrumentation for the
 * queries (INSTRUMENT_ROWS and INSTRUMENT_BUFFERS).
 *
 * The ExecutorEnd hook will look for every qual in the query, and
 * stores the quals of the form:
 *		- EXPR OPERATOR CONSTANT
 *		- EXPR OPERATOR EXPR
 *
 *
 * ExecutorEnd 在PG里的设计是回收执行器全局状态和状态节点
 *
 * 这个hook会记录涉及 上述两种形式的表达式 的query
 *
 *
 * If pg_stat_statements is available, the statistics will be
 * aggregated by queryid, and a not-normalized statement will be
 * stored for each different queryid. This can allow third part tools
 * to do some work on a real query easily.
 *
 * 在pg_stats_statements 的帮助下， 统计信息会根据queryid聚类，得到的统计结果是未归一化的？
 *
 * The implementation is heavily inspired by pg_stat_statements
 *
 * Copyright (c) 2014,2017 Ronan Dunklau
 * Copyright (c) 2018, The Powa-Team
 *-------------------------------------------------------------------------
 */
#include <limits.h>
#include <math.h>
#include "postgres.h"
#include "access/hash.h"
#include "access/htup_details.h"
#if PG_VERSION_NUM >= 90600
#include "access/parallel.h"
#endif
#if PG_VERSION_NUM >= 100000 && PG_VERSION_NUM < 110000
#include "catalog/pg_authid.h"
#endif
#if PG_VERSION_NUM >= 110000
#include "catalog/pg_authid_d.h"
#endif
#include "catalog/pg_class.h"
#include "catalog/pg_namespace.h"
#include "catalog/pg_operator.h"
#include "catalog/pg_type.h"
#include "commands/dbcommands.h"
#include "fmgr.h"
#include "funcapi.h"
#include "mb/pg_wchar.h"
#include "miscadmin.h"
#include "nodes/execnodes.h"
#include "nodes/nodeFuncs.h"
#include "nodes/makefuncs.h"
#include "optimizer/clauses.h"
#include "optimizer/planner.h"
#include "parser/analyze.h"
#include "parser/parse_node.h"
#include "parser/parsetree.h"
#include "postmaster/autovacuum.h"
#include "storage/ipc.h"
#include "storage/lwlock.h"
#if PG_VERSION_NUM >= 100000
#include "storage/shmem.h"
#endif
#include "utils/array.h"
#include "utils/builtins.h"
#include "utils/guc.h"
#include "utils/lsyscache.h"
#include "utils/memutils.h"
#include "utils/tuplestore.h"

PG_MODULE_MAGIC;

#define PGQS_NAME_COLUMNS 7		/* number of column added when using
								 * pg_qualstats_column SRF */
#define PGQS_USAGE_DEALLOC_PERCENT	5	/* free this % of entries at once */
#define PGQS_MAX_DEFAULT	1000	/* default pgqs_max value */
#define PGQS_MAX_SAMPLE_DEFAULT 1000 /* default pgqs_max_sample value */
#define PGQS_MAX_FILTER_DEFAULT 1000 /* default pgqs_max_sample value */
#define PGQS_SAMPLE_MINUTES_DEFAULT     5   /* default minutes between filtering */
#define PGQS_SAMPLE_RANKING_DEFAULT     5   /* default ranking between filtering */
// 这里的LOACL 和 20% 的目的是什么？
#define PGQS_MAX_LOCAL_ENTRIES	(pgqs_max * 0.2)	/* do not track more of
													 * 20% of possible entries
													 * in shared mem */

#define PGQS_CONSTANT_SIZE 800	/* Truncate constant representation at 80 */

// 说明需要row count 和 buffer usage
#define PGQS_FLAGS (INSTRUMENT_ROWS|INSTRUMENT_BUFFERS)

#define PGQS_RATIO	0
#define PGQS_NUM	1

#define PGQS_LWL_ACQUIRE(lock, mode) if (!pgqs_backend) { \
	LWLockAcquire(lock, mode); \
	}

#define PGQS_LWL_RELEASE(lock) if (!pgqs_backend) { \
	LWLockRelease(lock); \
	}

#if PG_VERSION_NUM < 140000
#define ParallelLeaderBackendId ParallelMasterBackendId
#endif

#define InvalidCmdType        7

/*
 * Extension version number, for supporting older extension versions' objects
 */
typedef enum pgqsVersion
{
	PGQS_V1_0 = 0,
	PGQS_V2_0
} pgqsVersion;

/* pgqs_qualstats operations */
typedef enum pgqsAction
{
    SAMPLE,
    COLLECT,
    FILTER,
} pgqsAction;

/*---- Function declarations ----*/

void		_PG_init(void);
void		_PG_fini(void);

extern Datum pg_qualstats_reset(PG_FUNCTION_ARGS);
extern Datum pg_qualstats(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_2_0(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_names(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_names_2_0(PG_FUNCTION_ARGS);
static Datum pg_qualstats_common(PG_FUNCTION_ARGS, pgqsVersion api_version,
								 bool include_names);
extern Datum pg_qualstats_example_query(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_example_queries(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_sample(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_adjust_sample(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_sample_ranking(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_sample_proportion(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_collect(PG_FUNCTION_ARGS);
extern Datum pg_qualstats_index(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(pg_qualstats_reset);
PG_FUNCTION_INFO_V1(pg_qualstats);
PG_FUNCTION_INFO_V1(pg_qualstats_2_0);
PG_FUNCTION_INFO_V1(pg_qualstats_names);
PG_FUNCTION_INFO_V1(pg_qualstats_names_2_0);
PG_FUNCTION_INFO_V1(pg_qualstats_example_query);
PG_FUNCTION_INFO_V1(pg_qualstats_example_queries);
PG_FUNCTION_INFO_V1(pg_qualstats_res_catalog);
PG_FUNCTION_INFO_V1(pg_qualstats_sample);
PG_FUNCTION_INFO_V1(pg_qualstats_adjust_sample);
PG_FUNCTION_INFO_V1(pg_qualstats_sample_ranking);
PG_FUNCTION_INFO_V1(pg_qualstats_sample_proportion);
PG_FUNCTION_INFO_V1(pg_qualstats_collect);
PG_FUNCTION_INFO_V1(pg_qualstats_index);


static void pgqs_backend_mode_startup(void);
static void pgqs_shmem_startup(void);
static void pgqs_ExecutorStart(QueryDesc *queryDesc, int eflags);
static void pgqs_ExecutorRun(QueryDesc *queryDesc,
				 ScanDirection direction,
#if PG_VERSION_NUM >= 90600
				 uint64 count
#else
				 long count
#endif
#if PG_VERSION_NUM >= 100000
				 , bool execute_once
#endif
);
static void pgqs_ExecutorFinish(QueryDesc *queryDesc);
static void pgqs_ExecutorEnd(QueryDesc *queryDesc);

static ExecutorStart_hook_type prev_ExecutorStart = NULL;
static ExecutorRun_hook_type prev_ExecutorRun = NULL;
static ExecutorFinish_hook_type prev_ExecutorFinish = NULL;
static ExecutorEnd_hook_type prev_ExecutorEnd = NULL;
static shmem_startup_hook_type prev_shmem_startup_hook = NULL;

static uint32 pgqs_hash_fn(const void *key, Size keysize);
static uint32 pgqs_sample_hash_fn(const void *key, Size keysize);

#if PG_VERSION_NUM < 90500
static uint32 pgqs_uint32_hashfn(const void *key, Size keysize);
#endif

static bool pgqs_backend = false;
static int	pgqs_query_size;
static int	pgqs_max = PGQS_MAX_DEFAULT;			/* max # statements to track */
static int	pgqs_sample_max = PGQS_MAX_SAMPLE_DEFAULT;			/* max # statements to pre-sampling */
static int	pgqs_filter_max = PGQS_MAX_FILTER_DEFAULT;			/* max # statements to pre-sampling */
static int	pgqs_sample_minutes = PGQS_SAMPLE_MINUTES_DEFAULT;			/* Minutes between filtering */
static int	pgqs_sample_ranking = PGQS_SAMPLE_RANKING_DEFAULT;			/* Ranking between filtering */
static double	pgqs_sample_proportion;			/* Proportion between filtering */
// TODO: 自动建index的时机待确定，目前还是手动输入
//static int	pgqs_index_minutes = 60;			/* Minutes between indexing */


static bool pgqs_track_pgcatalog;	/* track queries on pg_catalog */
static bool pgqs_all_collect = true;	/* collect all queries */
static bool pgqs_resolve_oids;	/* resolve oids */
static bool pgqs_enabled;
static bool pgqs_track_constants;
static double pgqs_sample_rate;
static char *pgqs_result_dir = NULL;
static int	pgqs_min_err_ratio;
static int	pgqs_min_err_num;
static uint32 query_is_sampled;	/* Is the current query sampled, per backend */
static int	nesting_level = 0;	/* Current nesting depth of ExecutorRun calls */
static instr_time starttime;
static double totaltime;
static bool pgqs_assign_sample_rate_check_hook(double *newval, void **extra, GucSource source);
#if PG_VERSION_NUM > 90600
static void pgqs_set_query_sampled(uint32 sample);
#endif
static uint32 pgqs_is_query_sampled(void);


// TODO: hash冲突问题？

/*---- Data structures declarations ----*/
typedef struct pgqsSharedState
{
#if PG_VERSION_NUM >= 90400
	LWLock	   *lock;			/* protects counters hashtable
								 * search/modification */
	LWLock	   *querylock;		/* protects query hashtable
								 * search/modification */
    LWLock     *samplelock;     /* protects sample hashtable
								 * search/modification */
    LWLock     *filterlock;     /* protects filter hashtable
								 * search/modification */
#else
	LWLockId	lock;			/* protects counters hashtable
								 * search/modification */
	LWLockId	querylock;		/* protects query hashtable
								 * search/modification */
#endif
#if PG_VERSION_NUM >= 90600
	LWLock	   *sampledlock;	/* protects sampled array search/modification */
	uint32 		sampled[FLEXIBLE_ARRAY_MEMBER]; /* should we sample this
												 * query? */
#endif
} pgqsSharedState;

/* Since cff440d368, queryid becomes a uint64 internally. */

#if PG_VERSION_NUM >= 110000
typedef uint64 pgqs_queryid;
#else
typedef uint32 pgqs_queryid;
#endif

// TODO: 设计预采样的结构
// TODO: 增加对INSERT的采集

typedef struct pgqsHashKey
{
	Oid			userid;			/* user OID */
	Oid			dbid;			/* database OID */
	pgqs_queryid queryid;		/* query identifier (if set by another plugin */
	uint32		uniquequalnodeid;	/* Hash of the const */
	uint32		uniquequalid;	/* Hash of the parent, including the consts */
	char		evaltype;		/* Evaluation type. Can be 'f' to mean a qual
								 * executed after a scan, or 'i' for an
								 * indexqual */
} pgqsHashKey;

typedef struct pgqsSampleHashKey
{
    Oid			userid;			/* user OID */
    Oid			dbid;			/* database OID */
    pgqs_queryid queryid;		/* query identifier (if set by another plugin */
} pgqsSampleHashKey;


typedef struct pgqsNames
{
	NameData	rolname;
	NameData	datname;
	NameData	lrelname;
	NameData	lattname;
	NameData	opname;
	NameData	rrelname;
	NameData	rattname;
} pgqsNames;

// TODO: 这里并没有记录query的类型（select、insert）等，需要扩展

typedef struct pgqsEntry
{
	pgqsHashKey key;
	CmdType     cmdType;        // 记录sql的类型（select、insert等），以便确定后续预测时各成本的权重
	Oid			lrelid;			/* LHS relation OID or NULL if not var */      // l rel id
	AttrNumber	lattnum;		/* LHS attribute Number or NULL if not var */  // l att num
	Oid			opoid;			/* Operator OID */                             // op oid
	Oid			rrelid;			/* RHS relation OID or NULL if not var */      // r rel id
	AttrNumber	rattnum;		/* RHS attribute Number or NULL if not var */  // r att num
	char		constvalue[PGQS_CONSTANT_SIZE]; /* Textual representation of
												 * the right hand constant, if
												 * any */
	uint32		qualid;			/* Hash of the parent AND expression if any, 0  // 什么意思？
								 * otherwise. */
	uint32		qualnodeid;		/* Hash of the node itself */                   // Hash结点自身的id？

	int64		count;			/* # of operator execution */                   // 执行运算子的数量？
	int64		nbfiltered;		/* # of lines discarded by the operator */      // 筛选程度？  怎么衡量的？
	int			position;		/* content position in query text */            // 这个有什么用？
	double		usage;			/* # of qual execution, used for deallocation */ // 用于分配？
	double		min_err_estim[2];	/* min estimation error ratio and num */     // 这四个数有什么用？
	double		max_err_estim[2];	/* max estimation error ratio and num */
	double		mean_err_estim[2];	/* mean estimation error ratio and num */
	double		sum_err_estim[2];	/* sum of variances in estimation error
									 * ratio and num */
	int64		occurences;		/* # of qual execution, 1 per query */          // 执行次数
} pgqsEntry;

typedef struct pgqsSampleEntry
{
    pgqsSampleHashKey key;
    CmdType     cmdType;        // 记录sql的类型（select、insert等），以便确定后续预测时各成本的权重
    pgqs_queryid queryid;		/* query identifier (if set by another plugin */

    int64		occurences;		/* # of qual execution, 1 per query */          // 执行次数
    double totaltime;
} pgqsSampleEntry;

typedef struct pgqsFilterEntry
{
    pgqsSampleHashKey key;
    bool iscollect;
    bool isfilter;
} pgqsFilterEntry;

// pgqsEntry 里用oid 记录了相关信息, pgqsNames则是对应的字符串名称
typedef struct pgqsEntryWithNames
{
	pgqsEntry	entry;
	pgqsNames	names;
} pgqsEntryWithNames;

typedef struct pgqsQueryStringHashKey
{
	pgqs_queryid queryid;
} pgqsQueryStringHashKey;

typedef struct pgqsQueryStringEntry
{
	pgqsQueryStringHashKey key;

	// 这段话如何理解？ querytext是柔性数组吗？
	// 把querytext 看成一个指针好了 —— 那为什么不直接写成指针的形式呢？
	/*
	 * Imperatively at the end of the struct This is actually of length
	 * query_size, which is track_activity_query_size
	 */
	char		querytext[1];
} pgqsQueryStringEntry;

/*
 * Transient state of the query tree walker - for the meaning of the counters,
 * see pgqsEntry comments.
 */
typedef struct pgqsWalkerContext
{

    CmdType     cmdType;        // 上下文中要记录sql类型
	pgqs_queryid queryId;
	List	   *rtable;
	PlanState  *planstate;
	PlanState  *inner_planstate;
	PlanState  *outer_planstate;
	List	   *outer_tlist;        // outer_planstates 的 plan 的 targetlist
	List	   *inner_tlist;
	List	   *index_tlist;
	uint32		qualid;
	uint32		uniquequalid;	/* Hash of the parent, including the consts */
	int64		count;
	int64		nbfiltered;
	double		err_estim[2];
	int			nentries;		/* number of entries found so far */
	char		evaltype;
	const char *querytext;
} pgqsWalkerContext;


static bool pgqs_whereclause_tree_walker(Node *node, pgqsWalkerContext *query);
static pgqsEntry *pgqs_process_opexpr(OpExpr *expr, pgqsWalkerContext *context);
static pgqsEntry *pgqs_process_scalararrayopexpr(ScalarArrayOpExpr *expr, pgqsWalkerContext *context);
static pgqsEntry *pgqs_process_booltest(BooleanTest *expr, pgqsWalkerContext *context);
static void pgqs_collectNodeStats(PlanState *planstate, List *ancestors, pgqsWalkerContext *context);
static void pgqs_collectMemberNodeStats(int nplans, PlanState **planstates, List *ancestors, pgqsWalkerContext *context);
static void pgqs_collectSubPlanStats(List *plans, List *ancestors, pgqsWalkerContext *context);
static uint32 hashExpr(Expr *expr, pgqsWalkerContext *context, bool include_const);
static void exprRepr(Expr *expr, StringInfo buffer, pgqsWalkerContext *context, bool include_const);
static void pgqs_set_planstates(PlanState *planstate, pgqsWalkerContext *context);
static Expr *pgqs_resolve_var(Var *var, pgqsWalkerContext *context);
static double elapsed_time(instr_time *starttime);

static void pgqs_entry_dealloc(void);
static void pgqs_sample_entry_dealloc(void);
static inline void pgqs_entry_init(pgqsEntry *entry);
static inline void pgqs_sample_entry_init(pgqsSampleEntry *entry);
static inline void pgqs_entry_copy_raw(pgqsEntry *dest, pgqsEntry *src);
static void pgqs_entry_err_estim(pgqsEntry *e, double *err_estim, int64 occurences);
static void pgqs_queryentry_dealloc(void);
static void pgqs_localentry_dealloc(int nvictims);
static void pgqs_fillnames(pgqsEntryWithNames *entry);

static Size pgqs_memsize(void);
#if PG_VERSION_NUM >= 90600
static Size pgqs_sampled_array_size(void);
#endif


/* Global Hash */
static HTAB *pgqs_hash = NULL;
static HTAB *pgqs_sample_hash = NULL;
static HTAB *pgqs_filter_hash = NULL;
static HTAB *pgqs_query_examples_hash = NULL;
static pgqsSharedState *pgqs = NULL;

/* Local Hash */
static HTAB *pgqs_localhash = NULL;


void
_PG_init(void)
{
	if (!process_shared_preload_libraries_in_progress)
	{
		elog(WARNING, "Without shared_preload_libraries, only current backend stats will be available.");
		pgqs_backend = true;
	}
	else
	{
		pgqs_backend = false;
		// 将之前的shmem_startup_hook 先存起来， 标记现在的pgqs_shmem_startup 为 shmem_startup_hook
		// 记录这个是为了unload时能还原之前的hook
		prev_shmem_startup_hook = shmem_startup_hook;
		shmem_startup_hook = pgqs_shmem_startup;
	}

	prev_ExecutorStart = ExecutorStart_hook;
	ExecutorStart_hook = pgqs_ExecutorStart;
	prev_ExecutorRun = ExecutorRun_hook;
	ExecutorRun_hook = pgqs_ExecutorRun;
	prev_ExecutorFinish = ExecutorFinish_hook;
	ExecutorFinish_hook = pgqs_ExecutorFinish;
	prev_ExecutorEnd = ExecutorEnd_hook;
	ExecutorEnd_hook = pgqs_ExecutorEnd;

	// 在pg中定义一个属性变量
	DefineCustomBoolVariable("pg_qualstats.enabled",
							 "Enable / Disable pg_qualstats",
							 NULL,
							 &pgqs_enabled,
							 true,
							 PGC_USERSET,
							 0,
							 NULL,
							 NULL,
							 NULL);

	DefineCustomBoolVariable("pg_qualstats.track_constants",
							 "Enable / Disable pg_qualstats constants tracking",
							 NULL,
							 &pgqs_track_constants,
							 true,
							 PGC_USERSET,
							 0,
							 NULL,
							 NULL,
							 NULL);

	// PGC_USERSET 表示其值能在任何时候被任何人修改， PGC_POSTMASTER表示其值只能在启动时设置（且只能来自conf文件或命令行）
	//
    DefineCustomIntVariable("pg_qualstats.max",
							"Sets the maximum number of statements tracked by pg_qualstats.",
							NULL,
							&pgqs_max,
							PGQS_MAX_DEFAULT,
							100,
							INT_MAX,
							pgqs_backend ? PGC_USERSET : PGC_POSTMASTER,
							0,
							NULL,
							NULL,
							NULL);

	// 记录oid只能在一开始就设置
	if (!pgqs_backend)
		DefineCustomBoolVariable("pg_qualstats.resolve_oids",
								 "Store names alongside the oid. Eats MUCH more space!",
								 NULL,
								 &pgqs_resolve_oids,
								 false,
								 PGC_POSTMASTER,
								 0,
								 NULL,
								 NULL,
								 NULL);


	// 为什么要追踪catalog里的quals？
	DefineCustomBoolVariable("pg_qualstats.track_pg_catalog",
							 "Track quals on system catalogs too.",
							 NULL,
							 &pgqs_track_pgcatalog,
							 false,
							 PGC_USERSET,
							 0,
							 NULL,
							 NULL,
							 NULL);

	// 负数时只能取-1，此时表示采样率为 1 / MaxConnection
	DefineCustomRealVariable("pg_qualstats.sample_rate",
							 "Sampling rate. 1 means every query, 0.2 means 1 in five queries",
							 NULL,
							 &pgqs_sample_rate,
							 -1,
							 -1,
							 1,
							 PGC_USERSET,
							 0,
							 pgqs_assign_sample_rate_check_hook,
							 NULL,
							 NULL);

	// 这两个数据记录的是实际执行和预测信息之间的误差，暂时没想到较好的用法
	DefineCustomIntVariable("pg_qualstats.min_err_estimate_ratio",
							"Error estimation ratio threshold to save quals",
							NULL,
							&pgqs_min_err_ratio,
							0,
							0,
							INT_MAX,
							PGC_USERSET,
							0,
							NULL,
							NULL,
							NULL);

	DefineCustomIntVariable("pg_qualstats.min_err_estimate_num",
							"Error estimation num threshold to save quals",
							NULL,
							&pgqs_min_err_num,
							0,
							0,
							INT_MAX,
							PGC_USERSET,
							0,
							NULL,
							NULL,
							NULL);

    DefineCustomStringVariable("pg_qualstats.result_directory",
                               gettext_noop("The catalog of saving recommendation process"),
                               NULL,
                               &pgqs_result_dir,
                               NULL,
                               PGC_SIGHUP, 0,
                               NULL, NULL, NULL);


    DefineCustomIntVariable("pg_qualstats.pre_sample_max",
                            "Sets the maximum number of statements tracked by pre-sampling.",
                            NULL,
                            &pgqs_sample_max,
                            PGQS_MAX_DEFAULT,
                            100,
                            INT_MAX,
                            PGC_POSTMASTER,
                            0,
                            NULL,
                            NULL,
                            NULL);

    DefineCustomIntVariable("pg_qualstats.filter_max",
                            "Sets the maximum number of statements tracked by filter.",
                            NULL,
                            &pgqs_filter_max,
                            PGQS_MAX_DEFAULT,
                            100,
                            INT_MAX,
                            pgqs_backend ? PGC_USERSET : PGC_POSTMASTER,
                            0,
                            NULL,
                            NULL,
                            NULL);

    DefineCustomBoolVariable("pg_qualstats.all_collect",
                             "Collect all queries on system.",
                             NULL,
                             &pgqs_all_collect,
                             false,
                             PGC_USERSET,
                             0,
                             NULL,
                             NULL,
                             NULL);

    DefineCustomIntVariable("pg_qualstats.sample_minutes",
                             "Minutes between filtering queries that need to be collected",
                            NULL,
                            &pgqs_sample_minutes,
                            PGQS_SAMPLE_MINUTES_DEFAULT,
                            1,
                            60,
                            PGC_USERSET,
                            0,
                            NULL,
                            NULL,
                            NULL);

    DefineCustomIntVariable("pg_qualstats.sample_ranking",
                            "Ranking between filtering queries that need to be collected",
                            NULL,
                            &pgqs_sample_ranking,
                            PGQS_SAMPLE_RANKING_DEFAULT,
                            1,
                            INT_MAX,
                            PGC_USERSET,
                            0,
                            NULL,
                            NULL,
                            NULL);

    DefineCustomRealVariable("pg_qualstats.sample_proportion",
                             "Proportion between filtering queries that need to be collected",
                             NULL,
                             &pgqs_sample_proportion,
                             0.2,
                             0.01,
                             1,
                             PGC_USERSET,
                             0,
                             pgqs_assign_sample_rate_check_hook,
                             NULL,
                             NULL);


	// 检查上述变量
	EmitWarningsOnPlaceholders("pg_qualstats");

	// 获取config中设置的值
	parse_int(GetConfigOption("track_activity_query_size", false, false),
			  &pgqs_query_size, 0, NULL);

	if (!pgqs_backend)
	{
	    // TODO: 申请一个新的HASH SPACE 存储预采样信息
        // 申请shmemSpace
        RequestAddinShmemSpace(pgqs_memsize());
#if PG_VERSION_NUM >= 90600
		// 申请指定数量的lock （为什么是3？）
		RequestNamedLWLockTranche("pg_qualstats", 5);
#else
		// pg里的light-weight lock 轻量锁
		RequestAddinLWLocks(4);
#endif
	}
	else
		pgqs_backend_mode_startup();
}

void
_PG_fini(void)
{
	/* Uninstall hooks. */
	// 将这些hook切换回之前的， 那如果同一个阶段要用到两个不同的hook的话，该怎么办？
	shmem_startup_hook = prev_shmem_startup_hook;
	ExecutorStart_hook = prev_ExecutorStart;
	ExecutorRun_hook = prev_ExecutorRun;
	ExecutorFinish_hook = prev_ExecutorFinish;
	ExecutorEnd_hook = prev_ExecutorEnd;
}


/*
 * Check that the sample ratio is in the correct interval
 */
static bool
pgqs_assign_sample_rate_check_hook(double *newval, void **extra, GucSource source)
{
	double		val = *newval;

	if ((val < 0 && val != -1) || (val > 1))
		return false;
	if (val == -1)
		*newval = 1. / MaxConnections;
	return true;
}

#if PG_VERSION_NUM >= 90600
// 设置相应的query（MyBackendId怎么来的？） 是否要被采样  —— MyBackendId 是PG提供的，表示不同的用户连接
// 下面两个函数是 设置/查询 不同进程处理的query是否要被sample
// 要看pg的进程模型，一个query一个进程？进程又是如何调用到这些函数的？
static void
pgqs_set_query_sampled(uint32 sample)
{
	/* the decisions should only be made in leader */
	Assert(!IsParallelWorker());

	/* not supported in backend mode */
	if (pgqs_backend)
		return;

	/* in worker processes we need to get the info from shared memory */
	LWLockAcquire(pgqs->sampledlock, LW_EXCLUSIVE);
	pgqs->sampled[MyBackendId] = sample;
	LWLockRelease(pgqs->sampledlock);
}
#endif


static uint32
pgqs_is_query_sampled(void)
{
#if PG_VERSION_NUM >= 90600
	uint32		sampled;

	// debug后，发现这里确实有问题
	// TODO： 应该改成pgqs对应sample的项才对，不然会漏一些采样，见 pgqs_ExecutorEnd 最开始的判断
	// 为什么不是返回 pgqs->sampled[MybackendId]呢？
	/* in leader we can just check the global variable */
	if (!IsParallelWorker())
		return query_is_sampled;

	/* not supported in backend mode */
	if (pgqs_backend)
		return false;

	/* in worker processes we need to get the info from shared memory */
	PGQS_LWL_ACQUIRE(pgqs->sampledlock, LW_SHARED);
	sampled = pgqs->sampled[ParallelLeaderBackendId];
	PGQS_LWL_RELEASE(pgqs->sampledlock);

	return sampled;
#else
	return query_is_sampled;
#endif
}

// 或许写log时能用到该函数中的内容
/*
 * Do catalog search to replace oids with corresponding objects name
 */
void
pgqs_fillnames(pgqsEntryWithNames *entry)
{
#if PG_VERSION_NUM >= 110000
#define GET_ATTNAME(r, a)	get_attname(r, a, false)
#else
#define GET_ATTNAME(r, a)	get_attname(r, a)
#endif

#if PG_VERSION_NUM >= 90500
	namestrcpy(&(entry->names.rolname), GetUserNameFromId(entry->entry.key.userid, true));
#else
	namestrcpy(&(entry->names.rolname), GetUserNameFromId(entry->entry.key.userid));
#endif
	namestrcpy(&(entry->names.datname), get_database_name(entry->entry.key.dbid));

	if (entry->entry.lrelid != InvalidOid)
	{
		namestrcpy(&(entry->names.lrelname),
				   get_rel_name(entry->entry.lrelid));
		namestrcpy(&(entry->names.lattname),
				   GET_ATTNAME(entry->entry.lrelid, entry->entry.lattnum));
	}

	if (entry->entry.opoid != InvalidOid)
		namestrcpy(&(entry->names.opname), get_opname(entry->entry.opoid));

	if (entry->entry.rrelid != InvalidOid)
	{
		namestrcpy(&(entry->names.rrelname),
				   get_rel_name(entry->entry.rrelid));
		namestrcpy(&(entry->names.rattname),
				   GET_ATTNAME(entry->entry.rrelid, entry->entry.rattnum));
	}
#undef GET_ATTNAME
}

/*
 * Request rows and buffers instrumentation if pgqs is enabled
 */
static void
pgqs_ExecutorStart(QueryDesc *queryDesc, int eflags)
{

	/* Setup instrumentation */
	if (pgqs_enabled)
	{
		/*
		 * For rate sampling, randomly choose top-level statement. Either all
		 * nested statements will be explained or none will.
		 */
		if (nesting_level == 0
#if PG_VERSION_NUM >= 90600
			&& (!IsParallelWorker())
#endif
			)
		{
            pgqsSampleHashKey key;
            pgqsFilterEntry *newEntry;
            bool found;



            /* TODO: 待考虑
            while (hash_get_num_entries(pgqs_filter_hash)
                   >= pgqs_filter_max)
                pgqs_sample_entry_dealloc();
            */

            if (pgqs_all_collect) {
                query_is_sampled = COLLECT;
            } else {

                memset(&key, 0, sizeof(pgqsSampleHashKey));
                key.userid = GetUserId();
                key.dbid = MyDatabaseId;
                key.queryid = queryDesc->plannedstmt->queryId;

                PGQS_LWL_ACQUIRE(pgqs->filterlock, LW_SHARED);
                newEntry = (pgqsFilterEntry *) hash_search(pgqs_filter_hash, &key, HASH_FIND, &found);
                if (!found) {
                    // 没有相关信息， 需要预采样
                    query_is_sampled = SAMPLE;
                } else {
                    if (newEntry->isfilter) {
                        // 过滤，既不预采样，也不收集信息
                        query_is_sampled = FILTER;
                    } else if (newEntry->iscollect) {
                        // 收集信息
                        query_is_sampled = COLLECT;
                    }
                }

                PGQS_LWL_RELEASE(pgqs->filterlock);
            }


#if PG_VERSION_NUM >= 90600
			pgqs_set_query_sampled(query_is_sampled);
#endif
		}

		if (pgqs_is_query_sampled() == COLLECT)
			queryDesc->instrument_options |= PGQS_FLAGS;
	}

	// 上面的代码是说如果pgqs_enable为true，则判断是否要将相关query采样
	// 而这一段是要调用一些该插件的hook以外的hook，如果pgqs_enable为true，则调用之前设置过的hook
	// 否则，调用默认hook（因为未被该插件的hook顶替过）
	// 这段似乎说明了同一阶段的多个hook是如何共同作用的？

    INSTR_TIME_SET_CURRENT(starttime);
    totaltime = 0;
	if (prev_ExecutorStart)
		prev_ExecutorStart(queryDesc, eflags);
	else
		standard_ExecutorStart(queryDesc, eflags);
}

/*
 * ExecutorRun hook: all we need do is track nesting depth
 */
static void
pgqs_ExecutorRun(QueryDesc *queryDesc,
				 ScanDirection direction,
#if PG_VERSION_NUM >= 90600
				 uint64 count
#else
				 long count
#endif
#if PG_VERSION_NUM >= 100000
				 ,bool execute_once
#endif
)
{
    // 这里的nesting_level++ 应该是想说明，下一个调用的prev_ExecutorRun是在下一层嵌套中
    // nesting_level 应该是指明当前调用的hook是在第几层调用中，不过nesting_level只在这个插件之中，如何与
    // 非pgql的hook交互呢？（简单来说，就是搞这个插件有什么用？）
    // 另外，其又如何知到prev_ExecutorRun 这个hook的参数列表呢？（这也是为何这里要用 错误处理 模式代码的原因？）
	nesting_level++;
	PG_TRY();
	{
		if (prev_ExecutorRun)
#if PG_VERSION_NUM >= 100000
			prev_ExecutorRun(queryDesc, direction, count, execute_once);
#else
			prev_ExecutorRun(queryDesc, direction, count);
#endif
		else
#if PG_VERSION_NUM >= 100000
			standard_ExecutorRun(queryDesc, direction, count, execute_once);
#else
			standard_ExecutorRun(queryDesc, direction, count);
#endif
		nesting_level--;
	}
	PG_CATCH();
	{
		nesting_level--;
		PG_RE_THROW();
	}
	PG_END_TRY();
}

/*
 * ExecutorFinish hook: all we need do is track nesting depth
 */
static void
pgqs_ExecutorFinish(QueryDesc *queryDesc)
{
	nesting_level++;
	PG_TRY();
	{
		if (prev_ExecutorFinish)
			prev_ExecutorFinish(queryDesc);
		else
			standard_ExecutorFinish(queryDesc);
		nesting_level--;
	}
	PG_CATCH();
	{
		nesting_level--;
		PG_RE_THROW();
	}
	PG_END_TRY();
}

/*
 * Save a non normalized query for the queryid if no one already exists, and
 * do all the stat collecting job
 */
static void
pgqs_ExecutorEnd(QueryDesc *queryDesc) {
    pgqsQueryStringHashKey queryKey;
    pgqsQueryStringEntry *queryEntry;
    bool found;

    if ((pgqs || pgqs_backend) && pgqs_enabled && pgqs_is_query_sampled() == COLLECT
        #if PG_VERSION_NUM >= 90600
        && (!IsParallelWorker())
        #endif

        // 不理解为什么要多加下面这一步的检测...
        // 多加 (queryDesc->instrument_options & PGQS_FLAGS) 的检测，是因为 query_is_sampled
        // 会被其他query 影响， 若一个query不采样，而 query_is_sampled 被其他query影响时，就要检查对应的
        // instrument_options 了

        // pgqs_is_query_sampled 对于执行的主线程来说，是直接返回全局变量query_is_sampled的，
        // 但query_is_sampled 如果被其他query影响，不就导致该query不被采样了吗？ 这又要如何解决？

           /*
               * multiple ExecutorStart/ExecutorEnd can be interleaved, so when sampling
               * is activated there's no guarantee that pgqs_is_query_sampled() will
               * only detect queries that were actually sampled (thus having the
               * required instrumentation set up).  To avoid such cases, we double check
               * that we have the required instrumentation set up.  That won't exactly
               * detect the sampled queries, but that should be close enough and avoid
               * adding to much complexity.
               */
        && (queryDesc->instrument_options & PGQS_FLAGS) == PGQS_FLAGS
            ) {
        // HASHCTL： 创建hashtable 所需的参数
        HASHCTL info;
        pgqsEntry *localentry;
        pgqsEntry *newEntry;
        // HASH_SEQ_STATUS : HTAB + curBucket + curEntry
        HASH_SEQ_STATUS local_hash_seq;
        pgqsWalkerContext *context = palloc(sizeof(pgqsWalkerContext));

        // TODO: 是在这里就确定cmdtype吗？ 子查询要如何处理？
        // 如： UPDATE ... FROM （SELECT ... WHERE EXPR） 这里的SELECT 中的EXPR 要算update还是select ？
        // pgqs_ExecutorEnd 在有子查询的query中，要执行几次？
        context->cmdType = queryDesc->operation;
        // 将要用到的信息抽离出来再加以组装
        context->queryId = queryDesc->plannedstmt->queryId;
        context->rtable = queryDesc->plannedstmt->rtable;
        context->count = 0;
        context->qualid = 0;
        context->uniquequalid = 0;
        context->nbfiltered = 0;
        context->evaltype = 0;
        context->nentries = 0;
        context->querytext = queryDesc->sourceText;
        queryKey.queryid = context->queryId;    // queryKey只有queryid

        /* keep an unnormalized query example for each queryid if needed */
        if (pgqs_track_constants) {
            /* Lookup the hash table entry with a shared lock. */
            PGQS_LWL_ACQUIRE(pgqs->querylock, LW_SHARED);

            // pgqs_query_examples_hash 这个HTAB 不是null 吗？ 什么时候有值的？
            // 这个HTAB 是pgql自己管理的HTAB，其作用是记录都sample了哪些query，以queryid为key
            queryEntry = (pgqsQueryStringEntry *) hash_search_with_hash_value(pgqs_query_examples_hash, &queryKey,
                                                                              context->queryId,
                                                                              HASH_FIND, &found);

            /* Create the new entry if not present */
            if (!found) {
                bool excl_found;

                /* Need exclusive lock to add a new hashtable entry - promote */
                // 放读获写
                PGQS_LWL_RELEASE(pgqs->querylock);
                PGQS_LWL_ACQUIRE(pgqs->querylock, LW_EXCLUSIVE);

                // 解除第一个trace的query， 也就是说是采用先进先出的策略（有改进空间？）
                // TODO: 这里的替换策略或许可以考虑更改
                while (hash_get_num_entries(pgqs_query_examples_hash) >= pgqs_max)
                    pgqs_queryentry_dealloc();

                // HASH_ENTER 如果没找到，会在Table中创建一个Entry
                queryEntry = (pgqsQueryStringEntry *) hash_search_with_hash_value(pgqs_query_examples_hash, &queryKey,
                                                                                  context->queryId,
                                                                                  HASH_ENTER, &excl_found);

                /* Make sure it wasn't added by another backend */
                if (!excl_found)
                    // 因为是指针的形式，所以能和HTAB中的内容同步
                    // queryEntry 相关内存在申请时，在后面多申请了 query_size大小的char的空间
                    strncpy(queryEntry->querytext, context->querytext, pgqs_query_size);
            }

            PGQS_LWL_RELEASE(pgqs->querylock);
        }

        // pgqs_localhash 和 pgqs_query_examples_hash 有什么区别？
        // pgqs_localhash 和 pgqs_hash有什么区别？ 两个Table的Entry是一样的，为什么一个要在local memory 而 一个在 global？
        // 而且localhash 的大小为 20% 的 global？ （local的 起到类似buffer的作用？）
        // pgqs_hash 创建时，还多了HASH_CONTEXT的参数
        // pgqs_localhash 是query解析后的HTAB（含oid等信息）
        // pgqs_query_examples_hash是queryid + querytext的HTAB
        /* create local hash table if it hasn't been created yet */
        if (!pgqs_localhash) {
            memset(&info, 0, sizeof(info));
            info.keysize = sizeof(pgqsHashKey);

            // 这里也许就是为什么解析oid要提前开的原因？
            if (pgqs_resolve_oids)
                info.entrysize = sizeof(pgqsEntryWithNames);
            else
                info.entrysize = sizeof(pgqsEntry);

            // ！这个函数解释的hash的计算组成
            info.hash = pgqs_hash_fn;

            // nelem 是期望元素数，如果htab是shared-memory的，那么在飞速写入时（on the fly），无法expand该table，故应该是一个较为贴合的值
            // 而 unshared table则可以expand，故选择较小的数较好（让其自然扩张）
            // 虽然整个插件是建立在共享内存中的，但这个table只给该插件用，故应为unshared table
            pgqs_localhash = hash_create("pgqs_localhash",
                                         50,
                                         &info,
                                         HASH_ELEM | HASH_FUNCTION);
        }

        /* retrieve quals informations, main work starts from here */
        // 这里收集执行后的信息，似乎根据推测生成tuple数与实际生成tuple数的差值来推荐？？？
        // ancestors 似乎一直是NULL？
        pgqs_collectNodeStats(queryDesc->planstate, NIL, context);

        /* if any quals found, store them in shared memory */
        if (context->nentries) {
            // ? 当pgqs_localhash的数量超过 20%的最大值时，要替换内容？
            // 20 % 是因为local hash 创建时只有50个elem 的size
            // pgqs_localhash 是本地hash table ，是存什么的？
            /*
             * Before acquiring exlusive lwlock, check if there's enough room
             * to store local hash.  Also, do not remove more than 20% of
             * maximum number of entries in shared memory (wether they are
             * used or not). This should not happen since we shouldn't store
             * that much entries in localhash in the first place.
             */

            // 什么样的情景才会除法 nvictims > 0 呢？
            // localhash 不都是每次用完就丢弃吗？
            // 另外，不用对localhash加锁吗？

            int nvictims = hash_get_num_entries(pgqs_localhash) -
                           PGQS_MAX_LOCAL_ENTRIES;

            // TODO: 二者的替换策略是否有所不同？
            // localhash 中是按 table的 顺序 选择victim的
            if (nvictims > 0)
                pgqs_localentry_dealloc(nvictims);

            PGQS_LWL_ACQUIRE(pgqs->lock, LW_EXCLUSIVE);

            // pgqs_hash 和 pgqs_localhash的区别是什么？？？
            // pgqs_hash 是按使用频率来选择victim的， 为什么有这个差异？
            while (hash_get_num_entries(pgqs_hash) +
                   hash_get_num_entries(pgqs_localhash) >= pgqs_max)
                pgqs_entry_dealloc();

            hash_seq_init(&local_hash_seq, pgqs_localhash);
            // hash_seq_search = NULL 说明遍历完了
            // 每次找到目标项之后，总是总是会先把localhash中的条目删去
            // 所以每次hash_seq_search总是一开始就能能找到目标 —— 但这样的话，为什么要比较nvictims呢？
            // 如果有两个expr的话，也许一个query就会在localhash中占两个？
            while ((localentry = hash_seq_search(&local_hash_seq)) != NULL) {
                newEntry = (pgqsEntry *) hash_search(pgqs_hash, &localentry->key,
                                                     HASH_ENTER, &found);

                if (!found) {
                    /* raw copy the local entry */
                    pgqs_entry_copy_raw(newEntry, localentry);
                } else {
                    // 如果本来就有，只要更新数据就好了
                    /* only update counters value */
                    newEntry->count += localentry->count;
                    newEntry->nbfiltered += localentry->nbfiltered;
                    // usage 是执行次数
                    newEntry->usage += localentry->usage;
                    /* compute estimation error min, max, mean and variance */
                    pgqs_entry_err_estim(newEntry, localentry->mean_err_estim,
                                         localentry->occurences);
                }
                /* cleanup local hash */

                hash_search(pgqs_localhash, &localentry->key, HASH_REMOVE, NULL);
            }

            PGQS_LWL_RELEASE(pgqs->lock);
        }
    }
    // TODO: 如果满足条件，则正常统计，否则，只统计时间信息
    //（没法简单地拿到uniquequalid的信息，只能根据queryid粗略统计）
    // 需要的话，可能要到优化完之后、执行前获取信息
    if (prev_ExecutorEnd)
        prev_ExecutorEnd(queryDesc);
    else
        standard_ExecutorEnd(queryDesc);

    if ((pgqs || pgqs_backend) && pgqs_enabled) {
        totaltime += elapsed_time(&starttime);
        if (pgqs_is_query_sampled() == FILTER) {
            // TODO: 过滤表应是用sql语句创建的，可持久化的；详细表则是存在内存中的
            // 在详细表 或 过滤表中不用统计

        } else if (pgqs_is_query_sampled() == SAMPLE) {
            // 预采样表
            pgqsSampleHashKey key;
            pgqsSampleEntry *newEntry;

            PGQS_LWL_ACQUIRE(pgqs->samplelock, LW_EXCLUSIVE);

            while (hash_get_num_entries(pgqs_sample_hash)
                   >= pgqs_sample_max)
                pgqs_sample_entry_dealloc();

            memset(&key, 0, sizeof(pgqsSampleHashKey));
            key.userid = GetUserId();
            key.dbid = MyDatabaseId;
            key.queryid = queryDesc->plannedstmt->queryId;

            newEntry = (pgqsSampleEntry *) hash_search(pgqs_sample_hash, &key, HASH_ENTER, &found);

            if (!found) {
                pgqs_sample_entry_init(newEntry);
                newEntry->queryid = queryDesc->plannedstmt->queryId;
                newEntry->cmdType = queryDesc->operation;
                newEntry->occurences = 1;
                newEntry->totaltime = totaltime;
            } else {
                newEntry->occurences += 1;
                newEntry->totaltime += totaltime;
            }

            PGQS_LWL_RELEASE(pgqs->samplelock);
        }
    }


}

/*
 * qsort comparator for sorting into increasing usage order
 */
static int
entry_cmp(const void *lhs, const void *rhs)
{
	double		l_usage = (*(pgqsEntry *const *) lhs)->usage;
	double		r_usage = (*(pgqsEntry *const *) rhs)->usage;

	if (l_usage < r_usage)
		return -1;
	else if (l_usage > r_usage)
		return +1;
	else
		return 0;
}

static int
entry_sample_cmp(const void *lhs, const void *rhs)
{
    double		l_totaltime = (*(pgqsSampleEntry *const *) lhs)->totaltime;
    double		r_totaltime = (*(pgqsSampleEntry *const *) rhs)->totaltime;

    if (l_totaltime < r_totaltime)
        return -1;
    else if (l_totaltime > r_totaltime)
        return +1;
    else
        return 0;
}

/*
 * Deallocate least used entries.
 * Caller must hold an exlusive lock on pgqs->lock
 */
static void
pgqs_entry_dealloc(void)
{
	HASH_SEQ_STATUS hash_seq;
	pgqsEntry **entries;
	pgqsEntry  *entry;
	int			nvictims;
	int			i;
	int			base_size;

	/*
	 * Sort entries by usage and deallocate PGQS_USAGE_DEALLOC_PERCENT of
	 * them. While we're scanning the table, apply the decay factor to the
	 * usage values.
	 */
	if (pgqs_resolve_oids)
		base_size = sizeof(pgqsEntryWithNames *);
	else
		base_size = sizeof(pgqsEntry *);

	entries = palloc(hash_get_num_entries(pgqs_hash) * base_size);

	i = 0;
	hash_seq_init(&hash_seq, pgqs_hash);
	while ((entry = hash_seq_search(&hash_seq)) != NULL)
	{
		entries[i++] = entry;
		entry->usage *= 0.99;
	}

	qsort(entries, i, base_size, entry_cmp);

	nvictims = Max(10, i * PGQS_USAGE_DEALLOC_PERCENT / 100);
	nvictims = Min(nvictims, i);

	for (i = 0; i < nvictims; i++)
		hash_search(pgqs_hash, &entries[i]->key, HASH_REMOVE, NULL);

	pfree(entries);
}

/*
 * Deallocate costing least totaltime entries.
 * Caller must hold an exlusive lock on pgqs->lock
 */
static void
pgqs_sample_entry_dealloc(void)
{
    HASH_SEQ_STATUS hash_seq;
    pgqsSampleEntry **entries;
    pgqsSampleEntry  *entry;
    int			nvictims;
    int			i;
    int			base_size;

    base_size = sizeof(pgqsSampleEntry *);

    entries = palloc(hash_get_num_entries(pgqs_sample_hash) * base_size);

    i = 0;
    hash_seq_init(&hash_seq, pgqs_sample_hash);
    while ((entry = hash_seq_search(&hash_seq)) != NULL)
    {
        entries[i++] = entry;
    }

    qsort(entries, i, base_size, entry_sample_cmp);

    nvictims = Max(10, i * PGQS_USAGE_DEALLOC_PERCENT / 100);
    nvictims = Min(nvictims, i);

    for (i = 0; i < nvictims; i++)
        hash_search(pgqs_sample_hash, &entries[i]->key, HASH_REMOVE, NULL);

    pfree(entries);
}

/* Initialize all non-key fields of the given entry. */
static inline void
pgqs_sample_entry_init(pgqsSampleEntry *entry)
{
	/* Note that pgqsNames if needed will be explicitly filled after this */
	memset(&(entry->cmdType), 0, sizeof(pgqsSampleEntry) - sizeof(pgqsSampleHashKey));
}

/* Initialize all non-key fields of the given sample_entry. */
static inline void
pgqs_entry_init(pgqsEntry *entry)
{
    /* Note that pgqsNames if needed will be explicitly filled after this */
    memset(&(entry->cmdType), 0, sizeof(pgqsEntry) - sizeof(pgqsHashKey));
}

/* Copy non-key and non-name fields from the given entry */
static inline void
pgqs_entry_copy_raw(pgqsEntry *dest, pgqsEntry *src)
{
	/* Note that pgqsNames if needed will be explicitly filled after this */
	memcpy(&(dest->cmdType),
		   &(src->cmdType),
		   (sizeof(pgqsEntry) - sizeof(pgqsHashKey)));
}

/*
 * Accurately compute estimation error ratio and num variance using Welford's
 * method. See <http://www.johndcook.com/blog/standard_deviation/>
 * Also maintain min and max values.
 */
static void
pgqs_entry_err_estim(pgqsEntry *e, double err_estim[2], int64 occurences)
{
	int		i;

	e->occurences += occurences;

	for (i = 0; i < 2; i++)
	{
		if ((e->occurences - occurences) == 0)
		{
			e->min_err_estim[i] = err_estim[i];
			e->max_err_estim[i] = err_estim[i];
			e->mean_err_estim[i] = err_estim[i];
		}
		else
		{
			double		old_err = e->mean_err_estim[i];

			e->mean_err_estim[i] +=
				(err_estim[i] - old_err) / e->occurences;
			e->sum_err_estim[i] +=
				(err_estim[i] - old_err) * (err_estim[i] - e->mean_err_estim[i]);
		}

		/* calculate min/max counters */
		if (e->min_err_estim[i] > err_estim[i])
			e->min_err_estim[i] = err_estim[i];
		if (e->max_err_estim[i] < err_estim[i])
			e->max_err_estim[i] = err_estim[i];
	}
}

/*
 * Deallocate the first example query.
 * Caller must hold an exlusive lock on pgqs->querylock
 */
static void
pgqs_queryentry_dealloc(void)
{
	HASH_SEQ_STATUS hash_seq;
	pgqsQueryStringEntry *entry;

	hash_seq_init(&hash_seq, pgqs_query_examples_hash);
	entry = hash_seq_search(&hash_seq);

	if (entry != NULL)
	{
		hash_search_with_hash_value(pgqs_query_examples_hash, &entry->key,
									entry->key.queryid, HASH_REMOVE, NULL);
		hash_seq_term(&hash_seq);
	}
}

/*
 * Remove the requested number of entries from pgqs_localhash/pgqs_sample_localhash.  Since the
 * entries are all coming from the same query, remove them without any specific
 * sort.
 */
static void
pgqs_localentry_dealloc(int nvictims)
{
	pgqsEntry  *localentry;
	HASH_SEQ_STATUS local_hash_seq;
	pgqsHashKey **victims;
	bool		need_seq_term = true;
	int			i,
				ptr = 0;

	if (nvictims <= 0)
		return;

	victims = palloc(sizeof(pgqsHashKey *) * nvictims);

	hash_seq_init(&local_hash_seq, pgqs_localhash);
	while (nvictims-- >= 0)
	{
		localentry = hash_seq_search(&local_hash_seq);

		/* check if caller required too many victims */
		if (!localentry)
		{
			need_seq_term = false;
			break;
		}

		victims[ptr++] = &localentry->key;
	}

	if (need_seq_term)
	    hash_seq_term(&local_hash_seq);

	for (i = 0; i < ptr; i++)
		hash_search(pgqs_localhash, victims[i], HASH_REMOVE, NULL);

	pfree(victims);
}

static void
pgqs_collectNodeStats(PlanState *planstate, List *ancestors, pgqsWalkerContext *context)
{
	Plan	   *plan = planstate->plan;
	Instrumentation *instrument = planstate->instrument;
	int64		oldcount = context->count;
	double		oldfiltered = context->nbfiltered;
	double		old_err_ratio = context->err_estim[PGQS_RATIO];
	double		old_err_num = context->err_estim[PGQS_NUM];
	double		total_filtered = 0;
	ListCell   *lc;
	List	   *parent = 0;
	List	   *indexquals = 0;
	List	   *quals = 0;

	context->planstate = planstate;

	/*
	 * We have to forcibly clean up the instrumentation state because we
	 * haven't done ExecutorEnd yet.  This is pretty grotty ...
	 */

	// 让pg收集完原始信息
	if (instrument)



	    // 这个函数的作用是让当前节点收集跑完一轮循环后的数据，然后更新统计信息！！！  这里或许能得到许多想要的数据
		InstrEndLoop(instrument);

	/* Retrieve the generic quals and indexquals */
	// ?? 源文件中qual的解释是 /* implicitly-ANDed qual conditions */ （隐式AND限定条件？）
	// 总之这里得到了 = 相关的信息
	switch (nodeTag(plan))
	{
		case T_IndexOnlyScan:
			indexquals = ((IndexOnlyScan *) plan)->indexqual;
			quals = plan->qual;
			break;
		case T_IndexScan:
			indexquals = ((IndexScan *) plan)->indexqualorig;
			quals = plan->qual;
			break;
		case T_BitmapIndexScan:
			indexquals = ((BitmapIndexScan *) plan)->indexqualorig;
			quals = plan->qual;
			break;
		case T_CteScan:
		case T_SeqScan:
		case T_BitmapHeapScan:
		case T_TidScan:
		case T_SubqueryScan:
		case T_FunctionScan:
		case T_ValuesScan:
		case T_WorkTableScan:
		case T_ForeignScan:
        case T_ModifyTable:
			quals = plan->qual;
			break;
		case T_NestLoop:
			quals = ((NestLoop *) plan)->join.joinqual;
			break;
		case T_MergeJoin:
			quals = ((MergeJoin *) plan)->mergeclauses;
			break;
		case T_HashJoin:
			quals = ((HashJoin *) plan)->hashclauses;
			break;
		default:
			break;

	}

    // 得到整个plan的state信息
	pgqs_set_planstates(planstate, context);

	// 为什么pg源代码中的List 要在 header的listcell 后面加多一个柔性数组?
	parent = list_union(indexquals, quals);
	if (list_length(parent) > 1)
	{
	    // Expr —— 在Executor中， 每个节点的第一个域用Expr表示
	    // 得到了两份hashvalue （分别是Const和非Const的  （这两个有什么区别吗？））
	    // uniquequalid 是要考虑等号右值的， 而qualid则是聚类后的编号
		context->uniquequalid = hashExpr((Expr *) parent, context, true);
		context->qualid = hashExpr((Expr *) parent, context, false);
	}

	//  double		nfiltered1;		/* # of tuples removed by scanqual or joinqual */
    //	double		nfiltered2;		/* # of tuples removed by "other" quals */

	total_filtered = instrument->nfiltered1 + instrument->nfiltered2;
	context->nbfiltered = total_filtered;

	//  double		ntuples;		/* total tuples produced */
    //	double		ntuples2;		/* secondary node-specific tuple counter */
    //  double		tuplecount;		/* # of tuples emitted so far this cycle */

    // 这是做什么？ count = 本轮产生的 + 整体共产生的（应该不包含这一轮？） +  总体过滤的
    // 在 InstrEndLoop 函数中， 会将tuplecount加到 ntuples中， 并将tuplecount归零，为啥这里要写这个？
    // 是不是因为程序执行间隙，tuplecount会更新？ —— 这样也不对，会导致这部分数据重复添加？
    // count记录的是该query总共涉及了多少tuple
	context->count = instrument->tuplecount + instrument->ntuples + total_filtered;

	// 这里应该是误差吧？
	// !!!! 该插件的误差衡量标准是比较预测的rows和实际的rows
	if (plan->plan_rows == instrument->ntuples)
	{
		context->err_estim[PGQS_RATIO] = 0;
		context->err_estim[PGQS_NUM] = 0;
	}
	else if (plan->plan_rows > instrument->ntuples)
	{
		/* XXX should use use a bigger value? */
		// 这里的bigger value 是指什么？
		if (instrument->ntuples == 0)
			context->err_estim[PGQS_RATIO] = plan->plan_rows * 1.0L;
		else
			context->err_estim[PGQS_RATIO] = plan->plan_rows * 1.0L / instrument->ntuples;
		context->err_estim[PGQS_NUM] = plan->plan_rows - instrument->ntuples;
	}
	else
	{
		/* plan_rows cannot be zero */
		// 该插件的错误估计率是绝对值（不反映pg结果多预测了还是少预测了）
		context->err_estim[PGQS_RATIO] = instrument->ntuples * 1.0L / plan->plan_rows;
		context->err_estim[PGQS_NUM] = instrument->ntuples - plan->plan_rows;
	}

	// ! 应该很重要？
	// 不清楚做了什么，也许更新了
	// 当收集到的数据超过一定程度才更新吗？
	if (context->err_estim[PGQS_RATIO] >= pgqs_min_err_ratio &&
		context->err_estim[PGQS_NUM] >= pgqs_min_err_num)
	{
	    // 之前context收集的信息都用在这里了

	    // TODO: 这里是无法收集到INSERT之类的原因
		/* Add the indexquals */
		// * expression_tree_walker() is designed to support routines that traverse
        // * a tree in a read-only fashion (although it will also work for routines
        // * that modify nodes in-place but never add/delete/replace nodes).
		context->evaltype = 'i';
		expression_tree_walker((Node *) indexquals,
							   pgqs_whereclause_tree_walker, context);

		/* Add the generic quals */
		context->evaltype = 'f';
		expression_tree_walker((Node *) quals, pgqs_whereclause_tree_walker,
							   context);
	}


	// 恢复成之前的上下文，不过这样的话统计信息不就没被保留吗？
	context->qualid = 0;
	context->uniquequalid = 0;
	context->count = oldcount;
	context->nbfiltered = oldfiltered;
	context->err_estim[PGQS_RATIO] = old_err_ratio;
	context->err_estim[PGQS_NUM] = old_err_num;

	// 收集子Node的数据
	// 执行不是自底向上的吗？ 这个 initplan要怎么理解？
	// /* Init SubPlanState nodes (un-correlated expr
    //								 * subselects) */
	foreach(lc, planstate->initPlan)
	{
		SubPlanState *sps = (SubPlanState *) lfirst(lc);
		pgqs_collectNodeStats(sps->planstate, ancestors, context);
	}

	// 这个的lefttree 和 righttree 又是指什么？
	/* lefttree */
	if (outerPlanState(planstate))
		pgqs_collectNodeStats(outerPlanState(planstate), ancestors, context);

	/* righttree */
	if (innerPlanState(planstate))
		pgqs_collectNodeStats(innerPlanState(planstate), ancestors, context);

	/* special child plans */
	switch (nodeTag(plan))
	{
#if PG_VERSION_NUM < 140000
		case T_ModifyTable:
			pgqs_collectMemberNodeStats(((ModifyTableState *) planstate)->mt_nplans,
										((ModifyTableState *) planstate)->mt_plans,
										ancestors, context);
			break;
#endif
		case T_Append:
			pgqs_collectMemberNodeStats(((AppendState *) planstate)->as_nplans,
										((AppendState *) planstate)->appendplans,
										ancestors, context);
			break;
		case T_MergeAppend:
			pgqs_collectMemberNodeStats(((MergeAppendState *) planstate)->ms_nplans,
										((MergeAppendState *) planstate)->mergeplans,
										ancestors, context);
			break;
		case T_BitmapAnd:
			pgqs_collectMemberNodeStats(((BitmapAndState *) planstate)->nplans,
										((BitmapAndState *) planstate)->bitmapplans,
										ancestors, context);
			break;
		case T_BitmapOr:
			pgqs_collectMemberNodeStats(((BitmapOrState *) planstate)->nplans,
										((BitmapOrState *) planstate)->bitmapplans,
										ancestors, context);
			break;
		case T_SubqueryScan:
			pgqs_collectNodeStats(((SubqueryScanState *) planstate)->subplan, ancestors, context);
			break;
		default:
			break;
	}

	/* subPlan-s */
	if (planstate->subPlan)
		pgqs_collectSubPlanStats(planstate->subPlan, ancestors, context);
}

static void
pgqs_collectMemberNodeStats(int nplans, PlanState **planstates,
							List *ancestors, pgqsWalkerContext *context)
{
	int			i;

	for (i = 0; i < nplans; i++)
		pgqs_collectNodeStats(planstates[i], ancestors, context);
}

static void
pgqs_collectSubPlanStats(List *plans, List *ancestors, pgqsWalkerContext *context)
{
	ListCell   *lst;

	foreach(lst, plans)
	{
		SubPlanState *sps = (SubPlanState *) lfirst(lst);

		pgqs_collectNodeStats(sps->planstate, ancestors, context);
	}
}

static pgqsEntry *
pgqs_process_scalararrayopexpr(ScalarArrayOpExpr *expr, pgqsWalkerContext *context)
{
	OpExpr	   *op = makeNode(OpExpr);
	int			len = 0;
	pgqsEntry  *entry = NULL;
	Expr	   *array = lsecond(expr->args);

	op->opno = expr->opno;
	op->opfuncid = expr->opfuncid;
	op->inputcollid = expr->inputcollid;
	op->opresulttype = BOOLOID;
	op->args = expr->args;
	switch (array->type)
	{
		case T_ArrayExpr:
			len = list_length(((ArrayExpr *) array)->elements);
			break;
		case T_Const:
			/* Const is an array. */
			{
				Const	   *arrayconst = (Const *) array;
				ArrayType  *array_type;

				if (arrayconst->constisnull)
					return NULL;

				array_type = DatumGetArrayTypeP(arrayconst->constvalue);

				if (ARR_NDIM(array_type) > 0)
					len = ARR_DIMS(array_type)[0];
			}
			break;
		default:
			break;
	}

	if (len > 0)
	{
		context->count *= len;
		entry = pgqs_process_opexpr(op, context);
	}

	return entry;
}

static pgqsEntry *
pgqs_process_booltest(BooleanTest *expr, pgqsWalkerContext *context)
{
	pgqsHashKey key;
	pgqsEntry  *entry;
	bool		found;
	Var		   *var;
	Expr	   *newexpr = NULL;
	char	   *constant;
	Oid			opoid;
	RangeTblEntry *rte;

	/* do not store more than 20% of possible entries in shared mem */
	if (context->nentries >= PGQS_MAX_LOCAL_ENTRIES)
		return NULL;

	if (IsA(expr->arg, Var))
		newexpr = pgqs_resolve_var((Var *) expr->arg, context);

	if (!(newexpr && IsA(newexpr, Var)))
		return NULL;

	var = (Var *) newexpr;
	rte = list_nth(context->rtable, var->varno - 1);
	switch (expr->booltesttype)
	{
		case IS_TRUE:
			constant = "TRUE::bool";
			opoid = BooleanEqualOperator;
			break;
		case IS_FALSE:
			constant = "FALSE::bool";
			opoid = BooleanEqualOperator;
			break;
		case IS_NOT_TRUE:
			constant = "TRUE::bool";
			opoid = BooleanNotEqualOperator;
			break;
		case IS_NOT_FALSE:
			constant = "FALSE::bool";
			opoid = BooleanNotEqualOperator;
			break;
		case IS_UNKNOWN:
			constant = "NULL::bool";
			opoid = BooleanEqualOperator;
			break;
		case IS_NOT_UNKNOWN:
			constant = "NULL::bool";
			opoid = BooleanNotEqualOperator;
			break;
		default:
			/* Bail out */
			return NULL;
	}
	memset(&key, 0, sizeof(pgqsHashKey));
	key.userid = GetUserId();
	key.dbid = MyDatabaseId;
	key.uniquequalid = context->uniquequalid;
	key.uniquequalnodeid = hashExpr((Expr *) expr, context, pgqs_track_constants);
	key.queryid = context->queryId;
	key.evaltype = context->evaltype;

	/* local hash, no lock needed */
	entry = (pgqsEntry *) hash_search(pgqs_localhash, &key, HASH_ENTER, &found);
	if (!found)
	{
		context->nentries++;

		// TODO: 添加cmdtype
		pgqs_entry_init(entry);
		entry->qualnodeid = hashExpr((Expr *) expr, context, false);
		entry->qualid = context->qualid;
		entry->opoid = opoid;

		if (rte->rtekind == RTE_RELATION)
		{
			entry->lrelid = rte->relid;
			entry->lattnum = var->varattno;
		}

		if (pgqs_track_constants)
		{
			char	   *utf8const = (char *) pg_do_encoding_conversion((unsigned char *) constant,
																	   strlen(constant),
																	   GetDatabaseEncoding(),
																	   PG_UTF8);

			strncpy(entry->constvalue, utf8const, strlen(utf8const));
		}
		else
			memset(entry->constvalue, 0, sizeof(char) * PGQS_CONSTANT_SIZE);

		if (pgqs_resolve_oids)
			pgqs_fillnames((pgqsEntryWithNames *) entry);
	}

	entry->nbfiltered += context->nbfiltered;
	entry->count += context->count;
	entry->usage += 1;
	/* compute estimation error min, max, mean and variance */
	pgqs_entry_err_estim(entry, context->err_estim, 1);

	return entry;
}

static void
get_const_expr(Const *constval, StringInfo buf)
{
	Oid			typoutput;
	bool		typIsVarlena;
	char	   *extval;

	if (constval->constisnull)
	{
		/*
		 * Always label the type of a NULL constant to prevent misdecisions
		 * about type when reparsing.
		 */
		appendStringInfoString(buf, "NULL");
		appendStringInfo(buf, "::%s",
						 format_type_with_typemod(constval->consttype,
												  constval->consttypmod));
		return;
	}

	getTypeOutputInfo(constval->consttype, &typoutput, &typIsVarlena);
	extval = OidOutputFunctionCall(typoutput, constval->constvalue);

	switch (constval->consttype)
	{
		case INT2OID:
		case INT4OID:
		case INT8OID:
		case OIDOID:
		case FLOAT4OID:
		case FLOAT8OID:
		case NUMERICOID:
			{
				/*
				 * These types are printed without quotes unless they contain
				 * values that aren't accepted by the scanner unquoted (e.g.,
				 * 'NaN').  Note that strtod() and friends might accept NaN,
				 * so we can't use that to test.
				 *
				 * In reality we only need to defend against infinity and NaN,
				 * so we need not get too crazy about pattern matching here.
				 *
				 * There is a special-case gotcha: if the constant is signed,
				 * we need to parenthesize it, else the parser might see a
				 * leading plus/minus as binding less tightly than adjacent
				 * operators --- particularly, the cast that we might attach
				 * below.
				 */
				if (strspn(extval, "0123456789+-eE.") == strlen(extval))
				{
					if (extval[0] == '+' || extval[0] == '-')
						appendStringInfo(buf, "(%s)", extval);
					else
						appendStringInfoString(buf, extval);
				}
				else
					appendStringInfo(buf, "'%s'", extval);
			}
			break;

		case BITOID:
		case VARBITOID:
			appendStringInfo(buf, "B'%s'", extval);
			break;

		case BOOLOID:
			if (strcmp(extval, "t") == 0)
				appendStringInfoString(buf, "true");
			else
				appendStringInfoString(buf, "false");
			break;

		default:
			appendStringInfoString(buf, quote_literal_cstr(extval));
			break;
	}

	pfree(extval);

	/*
	 * For showtype == 0, append ::typename unless the constant will be
	 * implicitly typed as the right type when it is read in.
	 */
	appendStringInfo(buf, "::%s",
					 format_type_with_typemod(constval->consttype,
											  constval->consttypmod));

}

/*-----------
 * In order to avoid duplicated entries for sementically equivalent OpExpr,
 * this function returns a canonical version of the given OpExpr.
 *
 * For now, the only modification is for OpExpr with a Var and a Const, we
 * prefer the form:
 * Var operator Const
 * with the Var on the LHS.  If the expression in the opposite form and the
 * operator has a commutator, we'll commute it, otherwise fallback to the
 * original OpExpr with the Var on the RHS.
 * OpExpr of the form Var operator Var can still be redundant.
 */
static OpExpr *
pgqs_get_canonical_opexpr(OpExpr *expr, bool *commuted)
{
	if (commuted)
		*commuted = false;

	/* Only OpExpr with 2 arguments needs special processing. */
	if (list_length(expr->args) != 2)
		return expr;

	/* If the 1st argument is a Var, nothing is done */
	if (IsA(linitial(expr->args), Var))
		return expr;

	/* If the 2nd argument is a Var, commute the OpExpr if possible */
	if (IsA(lsecond(expr->args), Var) && OidIsValid(get_commutator(expr->opno)))
	{
		OpExpr	   *new = copyObject(expr);

		CommuteOpExpr(new);

		if (commuted)
			*commuted = true;

		return new;
	}

	return expr;
}

static pgqsEntry *
pgqs_process_opexpr(OpExpr *expr, pgqsWalkerContext *context)
{
	/* do not store more than 20% of possible entries in shared mem */
	if (context->nentries >= PGQS_MAX_LOCAL_ENTRIES)
		return NULL;

	//List	   *args;			/* arguments to the operator (1 or 2) */
	if (list_length(expr->args) == 2)
	{
		bool		save_qual;
		Node	   *node;
		Var		   *var;
		Const	   *constant;
		Oid		   *sreliddest;
		AttrNumber *sattnumdest;
		pgqsEntry	tempentry;
		int			step;

		pgqs_entry_init(&tempentry);
		tempentry.opoid = expr->opno;
		tempentry.cmdType = context->cmdType;

		save_qual = false;
		var = NULL;				/* will store the last Var found, if any */
		constant = NULL;		/* will store the last Constant found, if any */

		/* setup the node and LHS destination fields for the 1st argument */
		node = linitial(expr->args);
		sreliddest = &(tempentry.lrelid);
		sattnumdest = &(tempentry.lattnum);

		for (step = 0; step < 2; step++)
		{
			if (IsA(node, RelabelType))
				node = (Node *) ((RelabelType *) node)->arg;

			if (IsA(node, Var))
				node = (Node *) pgqs_resolve_var((Var *) node, context);

			switch (node->type)
			{
				case T_Var:
					var = (Var *) node;
					{
						RangeTblEntry *rte;

						rte = list_nth(context->rtable, var->varno - 1);
						if (rte->rtekind == RTE_RELATION)
						{
							save_qual = true;
							*sreliddest = rte->relid;
							*sattnumdest = var->varattno;
						}
						else
							var = NULL;
					}
					break;
				case T_Const:
					constant = (Const *) node;
					break;
				default:
					break;
			}

			/* find the node to process for the 2nd pass */
			if (step == 0)
			{
				node = NULL;

				if (var == NULL)
				{
					bool		commuted;
					OpExpr	   *new = pgqs_get_canonical_opexpr(expr, &commuted);

					/*
					 * If the OpExpr was commuted we have to use the 1st
					 * argument of the new OpExpr, and keep using the LHS as
					 * destination fields.
					 */
					if (commuted)
					{
						Assert(sreliddest == &(tempentry.lrelid));
						Assert(sattnumdest == &(tempentry.lattnum));

						node = linitial(new->args);
					}
				}

				/*
				 * If the 1st argument was a var, or if it wasn't and the
				 * operator couldn't be commuted, use the 2nd argument and the
				 * RHS as destination fields.
				 */
				if (node == NULL)
				{
					/* simply process the next argument */
					node = lsecond(expr->args);

					/*
					 * a Var was found and stored on the LHS, so if the next
					 * node  will be stored on the RHS
					 */
					sreliddest = &(tempentry.rrelid);
					sattnumdest = &(tempentry.rattnum);
				}
			}
		}

		if (save_qual)
		{
			pgqsHashKey key;
			pgqsEntry  *entry;
			StringInfo	buf = makeStringInfo();
			bool		found;
			int			position = -1;

			/*
			 * If we don't track rels in the pg_catalog schema, lookup the
			 * schema to make sure its not pg_catalog. Otherwise, bail out.
			 */
			if (!pgqs_track_pgcatalog)
			{
				Oid			nsp;

				if (tempentry.lrelid != InvalidOid)
				{
					nsp = get_rel_namespace(tempentry.lrelid);

					Assert(OidIsValid(nsp));

					if (nsp == PG_CATALOG_NAMESPACE)
						return NULL;
				}

				if (tempentry.rrelid != InvalidOid)
				{
					nsp = get_rel_namespace(tempentry.rrelid);

					Assert(OidIsValid(nsp));

					if (nsp == PG_CATALOG_NAMESPACE)
						return NULL;
				}
			}

			if (constant != NULL && pgqs_track_constants)
			{
				get_const_expr(constant, buf);
				position = constant->location;
			}

			memset(&key, 0, sizeof(pgqsHashKey));
			key.userid = GetUserId();
			key.dbid = MyDatabaseId;
			key.uniquequalid = context->uniquequalid;
			key.uniquequalnodeid = hashExpr((Expr *) expr, context, pgqs_track_constants);
			key.queryid = context->queryId;
			key.evaltype = context->evaltype;

			/* local hash, no lock needed */
			entry = (pgqsEntry *) hash_search(pgqs_localhash, &key, HASH_ENTER,
											  &found);
			if (!found)
			{
				char	   *utf8const;
				int			len;

				context->nentries++;

				/* raw copy the temporary entry */
				pgqs_entry_copy_raw(entry, &tempentry);
				entry->position = position;
				entry->qualnodeid = hashExpr((Expr *) expr, context, false);
				entry->qualid = context->qualid;

				utf8const = (char *) pg_do_encoding_conversion((unsigned char *) buf->data,
															   strlen(buf->data),
															   GetDatabaseEncoding(),
															   PG_UTF8);
				len = strlen(utf8const);

				/*
				 * The const value can use multibyte characters, so we need to
				 * be careful when truncating the value.  Note that we need to
				 * use PG_UTF8 encoding explicitly here, as the value was just
				 * converted to this encoding.
				 */
				len = pg_encoding_mbcliplen(PG_UTF8, utf8const, len,
											PGQS_CONSTANT_SIZE - 1);

				memcpy(entry->constvalue, utf8const, len);
				entry->constvalue[len] = '\0';

				if (pgqs_resolve_oids)
					pgqs_fillnames((pgqsEntryWithNames *) entry);
			}

			entry->nbfiltered += context->nbfiltered;
			entry->count += context->count;
			entry->usage += 1;
			/* compute estimation error min, max, mean and variance */
			pgqs_entry_err_estim(entry, context->err_estim, 1);

			return entry;
		}
	}

	return NULL;
}

static bool
pgqs_whereclause_tree_walker(Node *node, pgqsWalkerContext *context)
{
	if (node == NULL)
		return false;

	switch (node->type)
	{
		case T_BoolExpr:
			{
				BoolExpr   *boolexpr = (BoolExpr *) node;

				if (boolexpr->boolop == NOT_EXPR)
				{
					/* Skip, and do not keep track of the qual */
					uint32		previous_hash = context->qualid;
					uint32		previous_uniquequalnodeid = context->uniquequalid;

					context->qualid = 0;
					context->uniquequalid = 0;
					expression_tree_walker((Node *) boolexpr->args, pgqs_whereclause_tree_walker, context);
					context->qualid = previous_hash;
					context->uniquequalid = previous_uniquequalnodeid;
					return false;
				}
				else if (boolexpr->boolop == OR_EXPR)
				{
					context->qualid = 0;
					context->uniquequalid = 0;
				}
				else if (boolexpr->boolop == AND_EXPR)
				{
					context->uniquequalid = hashExpr((Expr *) boolexpr, context, pgqs_track_constants);
					context->qualid = hashExpr((Expr *) boolexpr, context, false);
				}
				expression_tree_walker((Node *) boolexpr->args, pgqs_whereclause_tree_walker, context);
				return false;
			}
		case T_OpExpr:
			pgqs_process_opexpr((OpExpr *) node, context);
			return false;
		case T_ScalarArrayOpExpr:
			pgqs_process_scalararrayopexpr((ScalarArrayOpExpr *) node, context);
			return false;
		case T_BooleanTest:
			pgqs_process_booltest((BooleanTest *) node, context);
			return false;
		default:
			expression_tree_walker(node, pgqs_whereclause_tree_walker, context);
			return false;
	}
}

static void
pgqs_backend_mode_startup(void)
{
	HASHCTL		info;
	HASHCTL		queryinfo;
    HASHCTL     sampleinfo;
    HASHCTL     filterinfo;

	memset(&info, 0, sizeof(info));
	memset(&queryinfo, 0, sizeof(queryinfo));
    memset(&sampleinfo, 0, sizeof(sampleinfo));
    memset(&filterinfo, 0, sizeof(filterinfo));

    info.keysize = sizeof(pgqsHashKey);
	info.hcxt = TopMemoryContext;

    queryinfo.keysize = sizeof(pgqsQueryStringHashKey);
	queryinfo.entrysize = sizeof(pgqsQueryStringEntry) + pgqs_query_size * sizeof(char);
	queryinfo.hcxt = TopMemoryContext;

    sampleinfo.keysize = sizeof(pgqsSampleHashKey);
    sampleinfo.entrysize = sizeof(pgqsSampleEntry);
    sampleinfo.hcxt = TopMemoryContext;

    filterinfo.keysize = sizeof(pgqsSampleHashKey);
    filterinfo.entrysize = sizeof(pgqsFilterEntry);
    filterinfo.hcxt = TopMemoryContext;

	if (pgqs_resolve_oids)
		info.entrysize = sizeof(pgqsEntryWithNames);
	else
		info.entrysize = sizeof(pgqsEntry);

	info.hash = pgqs_hash_fn;
    sampleinfo.hash = pgqs_sample_hash_fn;
    filterinfo.hash = pgqs_sample_hash_fn;

	pgqs_hash = hash_create("pg_qualstatements_hash",
							  pgqs_max,
							  &info,
							  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);

    pgqs_sample_hash = hash_create("pg_qualsample_hash",
                                   pgqs_sample_max,
                                   &sampleinfo,
                                   HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);

    pgqs_filter_hash = hash_create("pg_qualfilter_hash",
                                   pgqs_filter_max,
                                   &sampleinfo,
                                   HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);


    pgqs_query_examples_hash = hash_create("pg_qualqueryexamples_hash",
											 pgqs_max,
											 &queryinfo,

/* On PG > 9.5, use the HASH_BLOBS optimization for uint32 keys. */
#if PG_VERSION_NUM >= 90500
											 HASH_ELEM | HASH_BLOBS | HASH_CONTEXT);
#else
											 HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
#endif
}

static void
pgqs_shmem_startup(void)
{
	HASHCTL		info;
	HASHCTL		queryinfo;
    HASHCTL     sampleinfo;
    HASHCTL     filterinfo;
	bool		found;

	Assert(!pgqs_backend);

	if (prev_shmem_startup_hook)
		prev_shmem_startup_hook();

	pgqs = NULL;
	LWLockAcquire(AddinShmemInitLock, LW_EXCLUSIVE);
	pgqs = ShmemInitStruct("pg_qualstats",
						   (sizeof(pgqsSharedState)
#if PG_VERSION_NUM >= 90600
							+ pgqs_sampled_array_size()
#endif
							),
						   &found);
	memset(&info, 0, sizeof(info));
	memset(&queryinfo, 0, sizeof(queryinfo));
    memset(&sampleinfo, 0, sizeof(sampleinfo));
    memset(&filterinfo, 0, sizeof(filterinfo));

	info.keysize = sizeof(pgqsHashKey);
	queryinfo.keysize = sizeof(pgqsQueryStringHashKey);
    queryinfo.entrysize = sizeof(pgqsQueryStringEntry) + pgqs_query_size * sizeof(char);
    sampleinfo.keysize = sizeof(pgqsSampleHashKey);
    sampleinfo.entrysize = sizeof(pgqsSampleEntry);
    filterinfo.keysize = sizeof(pgqsSampleHashKey);
    filterinfo.entrysize = sizeof(pgqsFilterEntry);

	if (pgqs_resolve_oids)
		info.entrysize = sizeof(pgqsEntryWithNames);
	else
		info.entrysize = sizeof(pgqsEntry);

	info.hash = pgqs_hash_fn;
    sampleinfo.hash = pgqs_sample_hash_fn;
    filterinfo.hash = pgqs_sample_hash_fn;
	if (!found)
	{
		/* First time through ... */
#if PG_VERSION_NUM >= 90600
		LWLockPadded *locks = GetNamedLWLockTranche("pg_qualstats");

		pgqs->lock = &(locks[0]).lock;
		pgqs->querylock = &(locks[1]).lock;
        pgqs->samplelock = &(locks[2]).lock;
        pgqs->filterlock = &(locks[3]).lock;
		pgqs->sampledlock = &(locks[4]).lock;
		/* mark all backends as not sampled */
		memset(pgqs->sampled, 0, pgqs_sampled_array_size());
#else
		pgqs->lock = LWLockAssign();
		pgqs->querylock = LWLockAssign();
#endif
	}
#if PG_VERSION_NUM < 90500
	queryinfo.hash = pgqs_uint32_hashfn;
#endif
	pgqs_hash = ShmemInitHash("pg_qualstatements_hash",
							  pgqs_max, pgqs_max,
							  &info,
							  HASH_ELEM | HASH_FUNCTION | HASH_FIXED_SIZE);

    pgqs_sample_hash = ShmemInitHash("pg_qualsample_hash",
                                     pgqs_sample_max,pgqs_sample_max,
                                     &sampleinfo,
                                     HASH_ELEM | HASH_FUNCTION | HASH_FIXED_SIZE);

    pgqs_filter_hash = ShmemInitHash("pg_qualfilter_hash",
                                     pgqs_filter_max, pgqs_filter_max,
                                     &filterinfo,
                                     HASH_ELEM | HASH_FUNCTION | HASH_FIXED_SIZE);

	pgqs_query_examples_hash = ShmemInitHash("pg_qualqueryexamples_hash",
											 pgqs_max, pgqs_max,
											 &queryinfo,

/* On PG > 9.5, use the HASH_BLOBS optimization for uint32 keys. */
#if PG_VERSION_NUM >= 90500
											 HASH_ELEM | HASH_BLOBS | HASH_FIXED_SIZE);
#else
											 HASH_ELEM | HASH_FUNCTION | HASH_FIXED_SIZE);
#endif
	LWLockRelease(AddinShmemInitLock);
}

Datum
pg_qualstats_reset(PG_FUNCTION_ARGS)
{
	HASH_SEQ_STATUS hash_seq;
	pgqsEntry  *entry;

	if ((!pgqs && !pgqs_backend) || !pgqs_hash)
	{
		ereport(ERROR,
				(errcode(ERRCODE_OBJECT_NOT_IN_PREREQUISITE_STATE),
				 errmsg("pg_qualstats must be loaded via shared_preload_libraries")));
	}

	PGQS_LWL_ACQUIRE(pgqs->lock, LW_EXCLUSIVE);

	hash_seq_init(&hash_seq, pgqs_hash);
	while ((entry = hash_seq_search(&hash_seq)) != NULL)
	{
		hash_search(pgqs_hash, &entry->key, HASH_REMOVE, NULL);
	}

	PGQS_LWL_RELEASE(pgqs->lock);
	PG_RETURN_VOID();
}

Datum
pg_qualstats_res_catalog(PG_FUNCTION_ARGS)
{

    if ((!pgqs && !pgqs_backend) || !pgqs_hash)
    {
        ereport(ERROR,
                (errcode(ERRCODE_OBJECT_NOT_IN_PREREQUISITE_STATE),
                        errmsg("pg_qualstats must be loaded via shared_preload_libraries")));
    }

    if (pgqs_result_dir == NULL)
        pgqs_result_dir = "/tmp/";

    PG_RETURN_TEXT_P(cstring_to_text(pgqs_result_dir));
}

/* Number of output arguments (columns) for various API versions */
#define PG_QUALSTATS_COLS_V1_0	18
#define PG_QUALSTATS_COLS_V2_0	27
#define PG_QUALSTATS_SAMPLE     4
#define PG_QUALSTATS_COLS		26	/* maximum of above */

/*
 * Retrieve statement statistics.
 *
 * The SQL API of this function has changed multiple times, and will likely
 * do so again in future.  To support the case where a newer version of this
 * loadable module is being used with an old SQL declaration of the function,
 * we continue to support the older API versions.  For 2.0.X and later, the
 * expected API version is identified by embedding it in the C name of the
 * function.  Unfortunately we weren't bright enough to do that for older
 * versions.
 */
Datum
pg_qualstats_2_0(PG_FUNCTION_ARGS)
{
	return pg_qualstats_common(fcinfo, PGQS_V2_0, false);
}

Datum
pg_qualstats_names_2_0(PG_FUNCTION_ARGS)
{
	return pg_qualstats_common(fcinfo, PGQS_V2_0, true);
}

Datum
pg_qualstats(PG_FUNCTION_ARGS)
{
	return pg_qualstats_common(fcinfo, PGQS_V1_0, false);
}

Datum
pg_qualstats_names(PG_FUNCTION_ARGS)
{
	return pg_qualstats_common(fcinfo, PGQS_V1_0, true);
}

Datum
pg_qualstats_common(PG_FUNCTION_ARGS, pgqsVersion api_version,
					bool include_names)
{
	ReturnSetInfo *rsinfo = (ReturnSetInfo *) fcinfo->resultinfo;
	int			nb_columns;
	TupleDesc	tupdesc;
	Tuplestorestate *tupstore;
	MemoryContext per_query_ctx;
	MemoryContext oldcontext;
	HASH_SEQ_STATUS hash_seq;
	Oid			userid = GetUserId();
	bool		is_allowed_role = false;
	pgqsEntry  *entry;
	Datum	   *values;
	bool	   *nulls;

#if PG_VERSION_NUM >= 140000
	/* Superusers or members of pg_read_all_stats members are allowed */
	is_allowed_role = is_member_of_role(GetUserId(), ROLE_PG_READ_ALL_STATS);
#elif PG_VERSION_NUM >= 100000
	/* Superusers or members of pg_read_all_stats members are allowed */
	is_allowed_role = is_member_of_role(GetUserId(), DEFAULT_ROLE_READ_ALL_STATS);
#else
	 /* Superusers are allowed */
	is_allowed_role = superuser();
#endif

	if ((!pgqs && !pgqs_backend) || !pgqs_hash)
		ereport(ERROR,
				(errcode(ERRCODE_OBJECT_NOT_IN_PREREQUISITE_STATE),
				 errmsg("pg_qualstats must be loaded via shared_preload_libraries")));

	/* check to see if caller supports us returning a tuplestore */
	if (rsinfo == NULL || !IsA(rsinfo, ReturnSetInfo))
		ereport(ERROR,
				(errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
				 errmsg("set-valued function called in context that cannot accept a set")));
	if (!(rsinfo->allowedModes & SFRM_Materialize))
		ereport(ERROR,
				(errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
				 errmsg("materialize mode required, but it is not " \
						"allowed in this context")));

	// 得到query文本内容
	per_query_ctx = rsinfo->econtext->ecxt_per_query_memory;
	// 得到之前的上下文信息
	oldcontext = MemoryContextSwitchTo(per_query_ctx);
	if (get_call_result_type(fcinfo, NULL, &tupdesc) != TYPEFUNC_COMPOSITE)
		elog(ERROR, "return type must be a row type");

	/* Check we have the expected number of output arguments. */
	switch (tupdesc->natts)
	{
		case PG_QUALSTATS_COLS_V1_0:
		case PG_QUALSTATS_COLS_V1_0 + PGQS_NAME_COLUMNS:
			if (api_version != PGQS_V1_0)
				elog(ERROR, "incorrect number of output arguments");
			break;
		case PG_QUALSTATS_COLS_V2_0:
		case PG_QUALSTATS_COLS_V2_0 + PGQS_NAME_COLUMNS:
			if (api_version != PGQS_V2_0)
				elog(ERROR, "incorrect number of output arguments");
			break;
		default:
			elog(ERROR, "incorrect number of output arguments");
	}

	tupstore = tuplestore_begin_heap(true, false, work_mem);
	rsinfo->returnMode = SFRM_Materialize;
	rsinfo->setResult = tupstore;
	rsinfo->setDesc = tupdesc;

	PGQS_LWL_ACQUIRE(pgqs->lock, LW_SHARED);
	hash_seq_init(&hash_seq, pgqs_hash);

	if (api_version == PGQS_V1_0)
		nb_columns = PG_QUALSTATS_COLS_V1_0;
	else
		nb_columns = PG_QUALSTATS_COLS_V2_0;

	if (include_names)
		nb_columns += PGQS_NAME_COLUMNS;

	Assert(nb_columns == tupdesc->natts);

    // TODO: 修改pg_qualstats的输出结果。
	values = palloc0(sizeof(Datum) * nb_columns);
	nulls = palloc0(sizeof(bool) * nb_columns);
	while ((entry = hash_seq_search(&hash_seq)) != NULL)
	{
		int			i = 0, j;
		double		stddev_estim[2];

		memset(values, 0, sizeof(Datum) * nb_columns);
		memset(nulls, 0, sizeof(bool) * nb_columns);
		values[i++] = ObjectIdGetDatum(entry->key.userid);
		values[i++] = ObjectIdGetDatum(entry->key.dbid);

		if (entry->cmdType != InvalidCmdType) {
            values[i++] = Int16GetDatum(entry->cmdType);
		}
		else
        {
		    nulls[i++] = true;
        }

		if (entry->lattnum != InvalidAttrNumber)
		{
			values[i++] = ObjectIdGetDatum(entry->lrelid);
			values[i++] = Int16GetDatum(entry->lattnum);
		}
		else
		{
			nulls[i++] = true;
			nulls[i++] = true;
		}
		values[i++] = Int32GetDatum(entry->opoid);
		if (entry->rattnum != InvalidAttrNumber)
		{
			values[i++] = ObjectIdGetDatum(entry->rrelid);
			values[i++] = Int16GetDatum(entry->rattnum);
		}
		else
		{
			nulls[i++] = true;
			nulls[i++] = true;
		}
		if (entry->qualid == 0)
			nulls[i++] = true;
		else
			values[i++] = Int64GetDatum(entry->qualid);

		if (entry->key.uniquequalid == 0)
			nulls[i++] = true;
		else
			values[i++] = Int64GetDatum(entry->key.uniquequalid);

		values[i++] = Int64GetDatum(entry->qualnodeid);
		values[i++] = Int64GetDatum(entry->key.uniquequalnodeid);
		values[i++] = Int64GetDatum(entry->occurences);
		values[i++] = Int64GetDatum(entry->count);
		values[i++] = Int64GetDatum(entry->nbfiltered);

		if (api_version >= PGQS_V2_0)
		{
			for (j = 0; j < 2; j++)
				{
					if (j == PGQS_RATIO)	/* min/max ratio are double precision */
					{
						values[i++] = Float8GetDatum(entry->min_err_estim[j]);
						values[i++] = Float8GetDatum(entry->max_err_estim[j]);
					}
					else				/* min/max num are bigint */
					{
						values[i++] = Int64GetDatum(entry->min_err_estim[j]);
						values[i++] = Int64GetDatum(entry->max_err_estim[j]);
					}
					values[i++] = Float8GetDatum(entry->mean_err_estim[j]);
					if (entry->occurences > 1)
						stddev_estim[j] = sqrt(entry->sum_err_estim[j] / entry->occurences);
					else
						stddev_estim[j] = 0.0;
					values[i++] = Float8GetDatumFast(stddev_estim[j]);
				}
		}

		if (entry->position == -1)
			nulls[i++] = true;
		else
			values[i++] = Int32GetDatum(entry->position);

		if (entry->key.queryid == 0)
			nulls[i++] = true;
		else
			values[i++] = Int64GetDatum(entry->key.queryid);

		if (entry->constvalue[0] != '\0')
		{
			if (is_allowed_role || entry->key.userid == userid)
			{
				values[i++] = CStringGetTextDatum((char *) pg_do_encoding_conversion(
							(unsigned char *) entry->constvalue,
							strlen(entry->constvalue),
							PG_UTF8,
							GetDatabaseEncoding()));
			}
			else
			{
				/*
				 * Don't show constant text, but hint as to the reason for not
				 * doing so
				 */
				values[i++] = CStringGetTextDatum("<insufficient privilege>");
			}
		}
		else
			nulls[i++] = true;

		if (entry->key.evaltype)
			values[i++] = CharGetDatum(entry->key.evaltype);
		else
			nulls[i++] = true;

		if (include_names)
		{
			if (pgqs_resolve_oids)
			{
				pgqsNames	names = ((pgqsEntryWithNames *) entry)->names;

				values[i++] = CStringGetTextDatum(NameStr(names.rolname));
				values[i++] = CStringGetTextDatum(NameStr(names.datname));
				values[i++] = CStringGetTextDatum(NameStr(names.lrelname));
				values[i++] = CStringGetTextDatum(NameStr(names.lattname));
				values[i++] = CStringGetTextDatum(NameStr(names.opname));
				values[i++] = CStringGetTextDatum(NameStr(names.rrelname));
				values[i++] = CStringGetTextDatum(NameStr(names.rattname));
			}
			else
			{
				for (; i < nb_columns; i++)
					nulls[i] = true;
			}
		}
		Assert(i == nb_columns);
		tuplestore_putvalues(tupstore, tupdesc, values, nulls);
	}

	PGQS_LWL_RELEASE(pgqs->lock);
	tuplestore_donestoring(tupstore);
	MemoryContextSwitchTo(oldcontext);

	return (Datum) 0;
}

Datum
pg_qualstats_sample(PG_FUNCTION_ARGS)
{
    ReturnSetInfo *rsinfo = (ReturnSetInfo *) fcinfo->resultinfo;
    int			nb_columns;
    TupleDesc	tupdesc;
    Tuplestorestate *tupstore;
    MemoryContext per_query_ctx;
    MemoryContext oldcontext;
    HASH_SEQ_STATUS hash_seq;
    pgqsSampleEntry *entry;
    Datum	   *values;
    bool	   *nulls;

    if ((!pgqs && !pgqs_backend) || !pgqs_sample_hash)
        ereport(ERROR,
                (errcode(ERRCODE_OBJECT_NOT_IN_PREREQUISITE_STATE),
                        errmsg("pg_qualstats must be loaded via shared_preload_libraries")));

    /* check to see if caller supports us returning a tuplestore */
    if (rsinfo == NULL || !IsA(rsinfo, ReturnSetInfo))
        ereport(ERROR,
                (errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
                        errmsg("set-valued function called in context that cannot accept a set")));
    if (!(rsinfo->allowedModes & SFRM_Materialize))
        ereport(ERROR,
                (errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
                        errmsg("materialize mode required, but it is not " \
						"allowed in this context")));

    // 得到query文本内容
    per_query_ctx = rsinfo->econtext->ecxt_per_query_memory;
    // 得到之前的上下文信息
    oldcontext = MemoryContextSwitchTo(per_query_ctx);
    if (get_call_result_type(fcinfo, NULL, &tupdesc) != TYPEFUNC_COMPOSITE)
        elog(ERROR, "return type must be a row type");

    /* Check we have the expected number of output arguments. */
    switch (tupdesc->natts)
    {
        case PG_QUALSTATS_SAMPLE:
            nb_columns = PG_QUALSTATS_SAMPLE;
            break;
        default:
            elog(ERROR, "incorrect number of output arguments");
    }

    tupstore = tuplestore_begin_heap(true, false, work_mem);
    rsinfo->returnMode = SFRM_Materialize;
    rsinfo->setResult = tupstore;
    rsinfo->setDesc = tupdesc;

    PGQS_LWL_ACQUIRE(pgqs->samplelock, LW_SHARED);
    hash_seq_init(&hash_seq, pgqs_sample_hash);

    Assert(nb_columns == tupdesc->natts);

    values = palloc0(sizeof(Datum) * nb_columns);
    nulls = palloc0(sizeof(bool) * nb_columns);
    while ((entry = hash_seq_search(&hash_seq)) != NULL)
    {
        int			i = 0;

        memset(values, 0, sizeof(Datum) * nb_columns);
        memset(nulls, 0, sizeof(bool) * nb_columns);


        if (entry->cmdType != InvalidCmdType) {
            values[i++] = Int16GetDatum(entry->cmdType);
        }
        else
        {
            nulls[i++] = true;
        }

        values[i++] = Int64GetDatumFast(entry->queryid);
        values[i++] = Int64GetDatum(entry->occurences);
        values[i++] = Float8GetDatum(entry->totaltime);


        Assert(i == nb_columns);
        tuplestore_putvalues(tupstore, tupdesc, values, nulls);
    }

    PGQS_LWL_RELEASE(pgqs->samplelock);
    tuplestore_donestoring(tupstore);
    MemoryContextSwitchTo(oldcontext);

    return (Datum) 0;
}

Datum
pg_qualstats_example_query(PG_FUNCTION_ARGS)
{
#if PG_VERSION_NUM >= 110000
	pgqs_queryid queryid = PG_GETARG_INT64(0);
#else
	pgqs_queryid queryid = PG_GETARG_UINT32(0);
#endif
	pgqsQueryStringEntry *entry;
	pgqsQueryStringHashKey queryKey;
	bool		found;

	if ((!pgqs && !pgqs_backend) || !pgqs_hash)
		ereport(ERROR,
				(errcode(ERRCODE_OBJECT_NOT_IN_PREREQUISITE_STATE),
				 errmsg("pg_qualstats must be loaded via shared_preload_libraries")));

	/* don't search the hash table if track_constants isn't enabled */
	if (!pgqs_track_constants)
		PG_RETURN_NULL();

	queryKey.queryid = queryid;

	PGQS_LWL_ACQUIRE(pgqs->querylock, LW_SHARED);
	// queryid = 0 为key？
	entry = hash_search_with_hash_value(pgqs_query_examples_hash, &queryKey,
										queryid, HASH_FIND, &found);
	PGQS_LWL_RELEASE(pgqs->querylock);

	if (found)
		PG_RETURN_TEXT_P(cstring_to_text(entry->querytext));
	else
		PG_RETURN_NULL();
}

Datum
pg_qualstats_example_queries(PG_FUNCTION_ARGS)
{
	ReturnSetInfo *rsinfo = (ReturnSetInfo *) fcinfo->resultinfo;
	TupleDesc	tupdesc;
	Tuplestorestate *tupstore;
	MemoryContext per_query_ctx;
	MemoryContext oldcontext;
	HASH_SEQ_STATUS hash_seq;
	pgqsQueryStringEntry *entry;

	if ((!pgqs && !pgqs_backend) || !pgqs_query_examples_hash)
		ereport(ERROR,
				(errcode(ERRCODE_OBJECT_NOT_IN_PREREQUISITE_STATE),
				 errmsg("pg_qualstats must be loaded via shared_preload_libraries")));

	/* check to see if caller supports us returning a tuplestore */
	if (rsinfo == NULL || !IsA(rsinfo, ReturnSetInfo))
		ereport(ERROR,
				(errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
				 errmsg("set-valued function called in context that cannot accept a set")));
	if (!(rsinfo->allowedModes & SFRM_Materialize))
		ereport(ERROR,
				(errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
				 errmsg("materialize mode required, but it is not " \
						"allowed in this context")));

	per_query_ctx = rsinfo->econtext->ecxt_per_query_memory;
	oldcontext = MemoryContextSwitchTo(per_query_ctx);

	if (get_call_result_type(fcinfo, NULL, &tupdesc) != TYPEFUNC_COMPOSITE)
		elog(ERROR, "return type must be a row type");

	tupstore = tuplestore_begin_heap(true, false, work_mem);
	rsinfo->returnMode = SFRM_Materialize;
	rsinfo->setResult = tupstore;
	rsinfo->setDesc = tupdesc;

	MemoryContextSwitchTo(oldcontext);

	/* don't need to scan the hash table if track_constants isn't enabled */
	if (!pgqs_track_constants)
		return (Datum) 0;

	PGQS_LWL_ACQUIRE(pgqs->querylock, LW_SHARED);
	hash_seq_init(&hash_seq, pgqs_query_examples_hash);

	while ((entry = hash_seq_search(&hash_seq)) != NULL)
	{
		Datum		values[2];
		bool		nulls[2];
		int64		queryid = entry->key.queryid;

		memset(values, 0, sizeof(values));
		memset(nulls, 0, sizeof(nulls));

		values[0] = Int64GetDatumFast(queryid);
		values[1] = CStringGetTextDatum(entry->querytext);

		tuplestore_putvalues(tupstore, tupdesc, values, nulls);

	}

	PGQS_LWL_RELEASE(pgqs->querylock);

	return (Datum) 0;
}

/* Select the query to be collected */
Datum
pg_qualstats_adjust_sample(PG_FUNCTION_ARGS)
{
    HASH_SEQ_STATUS hash_seq;
    pgqsSampleEntry **entries;
    pgqsSampleEntry  *entry;
    pgqsFilterEntry *filterEntry;
    int			nvictims;
    int			i;
    int			base_size;
    double      total_time = 0;

    base_size = sizeof(pgqsSampleEntry *);


    PGQS_LWL_ACQUIRE(pgqs->samplelock, LW_EXCLUSIVE);

    entries = palloc(hash_get_num_entries(pgqs_sample_hash) * base_size);

    i = 0;
    hash_seq_init(&hash_seq, pgqs_sample_hash);
    while ((entry = hash_seq_search(&hash_seq)) != NULL)
    {
        entries[i++] = entry;
    }

    qsort(entries, i, base_size, entry_sample_cmp);

    nvictims = Min(i, pgqs_sample_ranking);

    for (i = 0; i < nvictims; i++) {
        total_time += entries[i]->totaltime;
    }

    PGQS_LWL_ACQUIRE(pgqs->filterlock, LW_EXCLUSIVE);
    for (i = nvictims - 1; i >= 0; i--) {
        if (entries[i]->totaltime / total_time >= pgqs_sample_proportion) {
            hash_search(pgqs_sample_hash, &entries[i]->key, HASH_REMOVE, NULL);
            filterEntry = hash_search(pgqs_filter_hash, &entries[i]->key, HASH_ENTER, NULL);
            filterEntry->iscollect = true;
        } else {
            break;
        }
    }
    PGQS_LWL_RELEASE(pgqs->filterlock);
    PGQS_LWL_RELEASE(pgqs->samplelock);
    pfree(entries);
    PG_RETURN_VOID();
}

/* Change the sample ranking */
Datum
pg_qualstats_sample_ranking(PG_FUNCTION_ARGS) {
#if PG_VERSION_NUM >= 110000
    int ranking = PG_GETARG_INT64(0);
#else
    int ranking = PG_GETARG_UINT32(0);
#endif
    if (ranking >= 1) {
        pgqs_sample_ranking = ranking;
        PG_RETURN_BOOL(true);
    }

    PG_RETURN_BOOL(false);
}

/* Change the sample proportion */
Datum
pg_qualstats_sample_proportion(PG_FUNCTION_ARGS) {
#if PG_VERSION_NUM >= 110000
    double proportion = PG_GETARG_FLOAT8(0);
#else
    double proportion = PG_GETARG_FLOAT4(0);
#endif
    if (proportion >= 0.01 && proportion < 1) {
        pgqs_sample_proportion = proportion;
        PG_RETURN_BOOL(true);
    }

    PG_RETURN_BOOL(false);
}
/* Decide whether to collect all queries */
Datum
pg_qualstats_collect(PG_FUNCTION_ARGS) {
    pgqs_all_collect = !pgqs_all_collect;
    PG_RETURN_BOOL(pgqs_all_collect);
}

Datum
pg_qualstats_index(PG_FUNCTION_ARGS) {

    uint32 target_id = PG_GETARG_INT64(0);
    HASH_SEQ_STATUS hash_seq;
    pgqsEntry  *entry;

    if ((!pgqs && !pgqs_backend) || !pgqs_hash)
        ereport(ERROR,
                (errcode(ERRCODE_OBJECT_NOT_IN_PREREQUISITE_STATE),
                        errmsg("pg_qualstats must be loaded via shared_preload_libraries")));


    PGQS_LWL_ACQUIRE(pgqs->lock, LW_EXCLUSIVE);
    hash_seq_init(&hash_seq, pgqs_hash);

    while ((entry = hash_seq_search(&hash_seq)) != NULL) {
        if (entry->qualnodeid == target_id) {
            entry->key.evaltype = 'i';
        }
    }
    PGQS_LWL_RELEASE(pgqs->lock);
    PG_RETURN_VOID();
}

/*
 * Calculate hash value for a key
 */
static uint32
pgqs_hash_fn(const void *key, Size keysize)
{
	const pgqsHashKey *k = (const pgqsHashKey *) key;

    // 有必要将usreid 加入hashkey的考量吗？ -- 权限的原因？
    // 不同用户的相同query的queryid是一样的吗？ —— 是一样的
	return hash_uint32((uint32) k->userid) ^
		hash_uint32((uint32) k->dbid) ^
		hash_uint32((uint32) k->queryid) ^
		hash_uint32((uint32) k->uniquequalnodeid) ^
		hash_uint32((uint32) k->uniquequalid) ^
		hash_uint32((uint32) k->evaltype);
}

static uint32
pgqs_sample_hash_fn(const void *key, Size keysize)
{
    const pgqsSampleHashKey *k = (const pgqsSampleHashKey *) key;

    return hash_uint32((uint32) k->userid) ^
            hash_uint32((uint32) k->dbid) ^
            hash_uint32((uint32) k->queryid);
}


// 这个函数确定了整个planstate的链条
// planstate类型 和 outer、inner是如何区分的呢？ 有什么规律呢？
static void
pgqs_set_planstates(PlanState *planstate, pgqsWalkerContext *context)
{
	context->outer_tlist = NIL;
	context->inner_tlist = NIL;
	context->index_tlist = NIL;
	context->outer_planstate = NULL;
	context->inner_planstate = NULL;
	context->planstate = planstate;
	// IsA 类型判断
	if (IsA(planstate, AppendState))
		context->outer_planstate = ((AppendState *) planstate)->appendplans[0];
	else if (IsA(planstate, MergeAppendState))
		context->outer_planstate = ((MergeAppendState *) planstate)->mergeplans[0];
#if PG_VERSION_NUM < 140000
	else if (IsA(planstate, ModifyTableState))
		context->outer_planstate = ((ModifyTableState *) planstate)->mt_plans[0];
#endif
	else
		context->outer_planstate = outerPlanState(planstate);

	if (context->outer_planstate)
		context->outer_tlist = context->outer_planstate->plan->targetlist;
	else
		context->outer_tlist = NIL;

	if (IsA(planstate, SubqueryScanState))
		context->inner_planstate = ((SubqueryScanState *) planstate)->subplan;
	else if (IsA(planstate, CteScanState))
		context->inner_planstate = ((CteScanState *) planstate)->cteplanstate;
	else
		context->inner_planstate = innerPlanState(planstate);

	if (context->inner_planstate)
		context->inner_tlist = context->inner_planstate->plan->targetlist;
	else
		context->inner_tlist = NIL;
	/* index_tlist is set only if it's an IndexOnlyScan */
	if (IsA(planstate->plan, IndexOnlyScan))
		context->index_tlist = ((IndexOnlyScan *) planstate->plan)->indextlist;
#if PG_VERSION_NUM >= 90500
	else if (IsA(planstate->plan, ForeignScan))
		context->index_tlist = ((ForeignScan *) planstate->plan)->fdw_scan_tlist;
	else if (IsA(planstate->plan, CustomScan))
		context->index_tlist = ((CustomScan *) planstate->plan)->custom_scan_tlist;
#endif
	else
		context->index_tlist = NIL;
}

// 总而言之，这里解析了expr，得到了Const的表达式内容
// 得到的是最深层的var的Const？ 还是每次递归都有叠加？
static Expr *
pgqs_resolve_var(Var *var, pgqsWalkerContext *context)
{
	List	   *tlist = NULL;
	PlanState  *planstate = context->planstate;

	pgqs_set_planstates(context->planstate, context);

	// 在解析器和规划器中，varno 和 varattno 标识语义所指对象，这是一个基本关系列，除非引用是对连接使用列的引用，该列在语义上不等同于连接输入列
	// varno 指明类型 varrattno 表明是第几个attr
	// XXX_VAR 表明有相应的子树（对于整个表达式而言）
	switch (var->varno)
	{
		case INNER_VAR:
			tlist = context->inner_tlist;
			break;
		case OUTER_VAR:
			tlist = context->outer_tlist;
			break;
		case INDEX_VAR:
			tlist = context->index_tlist;
			break;
		default:
			return (Expr *) var;
	}
	if (tlist != NULL)
	{
	    // 这里得到了具体的项
		TargetEntry *entry = get_tle_by_resno(tlist, var->varattno);

		// in very many places it's convenient to process a whole query targetlist as a
        // single expression tree.
		if (entry != NULL)
		{
		    // entry -> expr : 要评估的表达式
			Var		   *newvar = (Var *) (entry->expr);

			if (var->varno == OUTER_VAR)
				pgqs_set_planstates(context->outer_planstate, context);

			if (var->varno == INNER_VAR)
				pgqs_set_planstates(context->inner_planstate, context);

			// 递归，看这个表达式是否还有下一层
			var = (Var *) pgqs_resolve_var(newvar, context);
		}
	}

	// #define IS_SPECIAL_VARNO(varno)		((varno) >= INNER_VAR)
	Assert(!(IsA(var, Var) && IS_SPECIAL_VARNO(var->varno)));

	/* If the result is something OTHER than a var, replace it by a constexpr */
	// 这个条件不是已经判断过了吗？
	if (!IsA(var, Var))
	{
		Const	   *consttext;

		// creates a Const node
		// 得到文本信息？   经过多次递归得到的var在这里整合
		// 假设有5层递归，是不是只有倒数第二层执行这部分才是有意义的？
		consttext = (Const *) makeConst(TEXTOID, -1, -1, -1, CStringGetTextDatum(nodeToString(var)), false, false);
		var = (Var *) consttext;
	}

	// 还原上下文的内容
	pgqs_set_planstates(planstate, context);

	return (Expr *) var;
}

/*
 * Estimate shared memory space needed.
 */
static Size
pgqs_memsize(void)
{
	Size		size;

	size = MAXALIGN(sizeof(pgqsSharedState));

	/*
	 * Add space for pre sampling and hash table
	 */
	size = add_size(size, hash_estimate_size(pgqs_sample_max, sizeof(pgqsSampleEntry)));
    size = add_size(size, hash_estimate_size(pgqs_filter_max, sizeof(pgqsFilterEntry)));

	if (pgqs_resolve_oids)
		size = add_size(size, hash_estimate_size(pgqs_max, sizeof(pgqsEntryWithNames)));
	else
		size = add_size(size, hash_estimate_size(pgqs_max, sizeof(pgqsEntry)));

	if (pgqs_track_constants)
	{
		/*
		 * In that case, we also need an additional struct for storing
		 * non-normalized queries.
		 */

		// 什么是non-normalized queries?
		// non-normalized queries 应该是指query会依表达式的右值常量不同而不同 （如 t1.attr1 = 1 和 t1.attr2 = 2会看成两个不同的query）
		// 显然，这样的内存占用是很大的
		size = add_size(size, hash_estimate_size(pgqs_max,
												 sizeof(pgqsQueryStringEntry) + pgqs_query_size * sizeof(char)));
	}
#if PG_VERSION_NUM >= 90600
	size = add_size(size, MAXALIGN(pgqs_sampled_array_size()));
#endif
	return size;
}

#if PG_VERSION_NUM >= 90600
static Size
pgqs_sampled_array_size(void)
{
	/*
	 * Parallel workers need to be sampled if their original query is also
	 * sampled.  We store in shared mem the sample state for each query,
	 * identified by their BackendId.  If need room for all possible backends,
	 * plus autovacuum launcher and workers, plus bg workers and an extra one
	 * since BackendId numerotation starts at 1.
	 */
	return (sizeof(bool) * (MaxConnections + autovacuum_max_workers + 1
							+ max_worker_processes + 1));
}
#endif

static uint32
hashExpr(Expr *expr, pgqsWalkerContext *context, bool include_const)
{
	StringInfo	buffer = makeStringInfo();

	exprRepr(expr, buffer, context, include_const);
	return hash_any((unsigned char *) buffer->data, buffer->len);

}

static void
exprRepr(Expr *expr, StringInfo buffer, pgqsWalkerContext *context, bool include_const)
{
	ListCell   *lc;

	if (expr == NULL)
		return;


	/*
	 * typedef struct Expr
	   {
             NodeTag		type;
	   } Expr;
	 *
	 */

	appendStringInfo(buffer, "%d-", expr->type);
	// * Var - expression node representing a variable (ie, a table column)
	// expr 除了Var以外，还能有什么type？
	if (IsA(expr, Var))
		expr = pgqs_resolve_var((Var *) expr, context);

	switch (expr->type)
	{
		case T_List:
			foreach(lc, (List *) expr)
				exprRepr((Expr *) lfirst(lc), buffer, context, include_const);

			break;
		case T_OpExpr:
			{
				OpExpr	   *opexpr;

				opexpr = pgqs_get_canonical_opexpr((OpExpr *) expr, NULL);

				appendStringInfo(buffer, "%d", opexpr->opno);
				exprRepr((Expr *) opexpr->args, buffer, context, include_const);
				break;
			}
		case T_Var:
			{
				Var		   *var = (Var *) expr;

				RangeTblEntry *rte = list_nth(context->rtable, var->varno - 1);

				if (rte->rtekind == RTE_RELATION)
					appendStringInfo(buffer, "%d;%d", rte->relid, var->varattno);
				else
					appendStringInfo(buffer, "NORTE%d;%d", var->varno, var->varattno);
			}
			break;
		case T_BoolExpr:
			appendStringInfo(buffer, "%d", ((BoolExpr *) expr)->boolop);
			exprRepr((Expr *) ((BoolExpr *) expr)->args, buffer, context, include_const);
			break;
		case T_BooleanTest:
			if (include_const)
				appendStringInfo(buffer, "%d", ((BooleanTest *) expr)->booltesttype);

			exprRepr((Expr *) ((BooleanTest *) expr)->arg, buffer, context, include_const);
			break;
		case T_Const:
			if (include_const)
				get_const_expr((Const *) expr, buffer);
			else
				appendStringInfoChar(buffer, '?');

			break;
		case T_CoerceViaIO:
			exprRepr((Expr *) ((CoerceViaIO *) expr)->arg, buffer, context, include_const);
			appendStringInfo(buffer, "|%d", ((CoerceViaIO *) expr)->resulttype);
			break;
		case T_FuncExpr:
			appendStringInfo(buffer, "|%d(", ((FuncExpr *) expr)->funcid);
			exprRepr((Expr *) ((FuncExpr *) expr)->args, buffer, context, include_const);
			appendStringInfoString(buffer, ")");
			break;
		case T_MinMaxExpr:
			appendStringInfo(buffer, "|minmax%d(", ((MinMaxExpr *) expr)->op);
			exprRepr((Expr *) ((MinMaxExpr *) expr)->args, buffer, context, include_const);
			appendStringInfoString(buffer, ")");
			break;

		default:
			appendStringInfoString(buffer, nodeToString(expr));
	}
}

/* Compute elapsed time in seconds since given timestamp */
static double
elapsed_time(instr_time *starttime)
{
    instr_time	endtime;

    INSTR_TIME_SET_CURRENT(endtime);
    INSTR_TIME_SUBTRACT(endtime, *starttime);
    return INSTR_TIME_GET_DOUBLE(endtime);
}

#if PG_VERSION_NUM < 90500
static uint32
pgqs_uint32_hashfn(const void *key, Size keysize)
{
	return ((pgqsQueryStringHashKey *) key)->queryid;
}
#endif
